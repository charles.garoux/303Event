<?php
Route::get('/', function () {return redirect(route('303Event.accueil'));});

/////////// Joueurs ///////////
Route::get('connexion', 'ConnexionJoueur@formulaire')->name("303Event.joueur.connexion.formulaire");
Route::post('connexion', 'ConnexionJoueur@vérification')->name("303Event.joueur.connexion.vérification");
Route::get('déconnexion', 'ConnexionJoueur@déconnexion')->name("303Event.joueur.déconnexion");

Route::get('inscription-nouveau-joueur', "inscriptionNouveauJoueur@formulaire")->name("303Event.joueur.nouveau.formulaire");
Route::post('inscription-nouveau-joueur', "inscriptionNouveauJoueur@ajout")->name("303Event.joueur.nouveau.ajout");

Route::get('informations/conditions-utilisation', function(){return view("conditionsUtilisation");})->name("303Event.informations.conditionsUtilisation");

/////////// Réactualisation de compte Joueurs ///////////
Route::get('réactualisation', 'ConnexionJoueur@réactualisationFormulaire')->name('303Event.joueur.réactualisation.formulaire');
Route::post('réactualisation', 'ConnexionJoueur@réactualisationVérification')->name('303Event.joueur.réactualisation.vérification');

/////////// Récupération de compte Joueurs ///////////
Route::get('recuperation-de-compte', 'RécupérationDeCompteJoueur@demandeFormulaire')->name('303Event.joueur.récupération.demande.formulaire');
Route::post('recuperation-de-compte', 'RécupérationDeCompteJoueur@traitementDemande')->name('303Event.joueur.récupération.demande.traitement');

Route::get('recuperation-de-compte/cle={cleDeRecuperation}', 'RécupérationDeCompteJoueur@récupérationFormulaire')->name('303Event.joueur.récupération.récupération.formulaire');
Route::post('recuperation-de-compte/cle={cleDeRecuperation}', 'RécupérationDeCompteJoueur@traitementRécupération')->name('303Event.joueur.récupération.récupération.traitement');

//Route::get('public/événement/fiche/{idEvenement}', 'EvénementController@fichePublicEvénement')->name("303Event.public.événement.fiche");

Route::group(["middleware" => 'AuthentificationJoueur'], function() {
	
	Route::get('ban', 'ConnexionJoueur@ban')->name("303Event.joueur.ban");

	Route::group(["middleware" => 'VérificationBan'], function() {

		Route::get('accueil', function(){return view("accueil");})->name("303Event.accueil");

		Route::get('team/inscription', 'TeamController@formulaireAjout')->name("303Event.team.nouvelle.formulaire");
		Route::post('team/inscription', 'TeamController@ajoutTeam')->name("303Event.team.nouvelle.ajout");

		Route::get('team/fiche', 'TeamController@fiche')->name('303Event.joueur.team.fiche');

		Route::post('team/véhicule/ajout', 'TeamController@ajoutNouveauVéhicule')->name('303Event.joueur.team.véhicule.ajout');
		Route::delete('team/véhicule/suppression/{idVehicule}', 'TeamController@ajoutSuppressionVéhicule')->name('303Event.joueur.team.véhicule.suppression');

		/////////// Evénement Joueurs ///////////
		Route::get('événement/liste', 'EvénementController@listeEvénements')->name("303Event.événement.liste");
		Route::post('événement/liste/recherche', 'EvénementController@recherche')->name("303Event.événement.liste.recherche");
		Route::get('événement/fiche/{idEvenement}', 'EvénementController@ficheEvénement')->name("303Event.événement.fiche");
		Route::post('événement/fiche/{idEvenement}', 'EvénementController@inscription')->name("303Event.événement.inscription");
		Route::post('événement/fiche/{idEvenement}/annulation', 'EvénementController@annulationInscription')->name("303Event.événement.inscription.annulation");
	});
});

/////////// Administration ///////////
Route::get('administration/connexion', 'ConnexionStaff@formulaire')->name("303Event.administration.connexion.formulaire");
Route::post('administration/connexion', 'ConnexionStaff@vérification')->name("303Event.administration.connexion.vérification");

Route::group(["middleware" => 'AuthentificationStaff'], function() {

	Route::get('administration/déconnexion', 'ConnexionStaff@déconnexion')->name("303Event.administration.déconnexion");

	/////////// Administration des joueurs ///////////
	Route::get('administration/liste-joueurs', 'Administration@listeJoueurs')->name("303Event.administration.liste.joueurs");
	Route::post('administration/liste-joueurs', 'Administration@recherche')->name("303Event.administration.liste.joueurs.recherche");

	Route::group(["middleware" => 'AuthentificationAdministrateur'], function() {
		Route::delete('administration/liste-joueurs/{idJoueur}', 'Administration@suppressionJoueur')->name("303Event.administration.liste.joueurs.suppression");

		Route::post('administration/liste-joueurs/expiration/{idJoueur}', 'Administration@expirationJoueur')->name("303Event.administration.liste.joueurs.expiration");
	});

	Route::post('administration/ban', 'Administration@bannir')->name("303Event.administration.banJoueur");
	Route::post('administration/déban', 'Administration@débannir')->name("303Event.administration.banJoueur.déban");

	Route::post('administration/note' , 'Administration@noter')->name("303Event.administration.notation");
	Route::post('administration/note/actualiser' , 'Administration@actualiserNotes')->name("303Event.administration.notation.actualiser");
	/////////// Team ///////////
	Route::get('administration/team/liste', 'Administration@listeTeams')->name("303Event.administration.team.liste");
	Route::delete('administration/team/éjecter/joueur/{idJoueur}', 'Administration@éjecterMembre')->name("303Event.administration.team.éjecter.joueur");
	Route::group(["middleware" => 'AuthentificationAdministrateur'], function() {
		Route::delete('administration/team/suppression/{idTeam}', 'Administration@suppressionTeam')->name("303Event.administration.team.suppression");
	});

	/////////// Administration interne ///////////
	Route::group(["middleware" => 'AuthentificationAdministrateur'], function() {
		Route::get('administration/liste-staff', 'AdministrationInterne@listeStaff')->name("303Event.administration.liste.staff");
		Route::get('administration/ajout-nouveau-staff', 'AdministrationInterne@formulaireAjout')->name("303Event.administration.nouveau.staff.formulaire");
		Route::post('administration/ajout-nouveau-staff', 'AdministrationInterne@ajout')->name("303Event.administration.nouveau.staff.ajout");
		Route::delete('administration/liste-staff/{idStaff}', 'AdministrationInterne@suppressionStaff')->name("303Event.administration.liste.staff.suppressionStaff");
	});

	/////////// Evénement Administration ///////////
	Route::get('administration/événement/création', 'AdministrationEvénement@formulaireCréation')->name("303Event.administration.événement.formulaireCréation");
	Route::post('administration/événement/création', 'AdministrationEvénement@créationEvénement')->name("303Event.administration.événement.créationEvénement");

	Route::get('administration/événement/fiche/{idEvenement}', 'AdministrationEvénement@ficheEvénement')->name('303Event.administration.événement.fiche');
	/////////// Equipe ///////////
	Route::get('administration/événement/équipe/liste', 'AdministrationEvénement@listeEquipes')->name('303Event.administration.événement.équipe.liste');
	Route::delete('administration/événement/équipe/suppression/{idEquipe}', 'AdministrationEvénement@suppressionEquipe')->name('303Event.administration.événement.équipe.suppression');
	Route::get('administration/événement/équipe/ajout', 'AdministrationEvénement@formulaireAjoutEquipe')->name('303Event.administration.événement.équipe.formulaire');
	Route::post('administration/événement/équipe/ajout', 'AdministrationEvénement@ajoutEquipe')->name('303Event.administration.événement.équipe.ajout');

	Route::group(["middleware" => 'AuthentificationAdministrateur'], function() {
		Route::delete('administration/événement/suppression/{idEvenement}', 'AdministrationEvénement@suppressionEvénement')->name('303Event.administration.événement.suppression');
	});

	Route::post('administration/événement/changer-état-inscription', 'AdministrationEvénement@changerEtatInscription')->name('303Event.administration.événement.inscription.changer.etat');
	
	/////////// Récupération Joueur ///////////
	Route::group(["middleware" => 'AuthentificationAdministrateur'], function() {
		Route::get('administration/joueur/recuperation-de-compte', 'AdministrationRécupération@demandeFormulaireJoueur')->name('303Event.administration.joueur.récupération.demande.formulaire');
		Route::post('administration/joueur/recuperation-de-compte', 'AdministrationRécupération@traitementDemandeJoueur')->name('303Event.administration.joueur.récupération.demande.traitement');
	});
});
