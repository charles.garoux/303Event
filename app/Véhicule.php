<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Véhicule extends Model
{
    protected $table = "Véhicule";

    public $timestamps = false;

    protected $fillable=['nom', 'type', 'joueur', 'idTeam'];
}
