<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Staff;

class Staff extends Model
{
    protected $table = "Staff";

    public $timestamps = false;

    protected $fillable=['identifiant', 'motDePasse', 'rang'];
}