<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evénement extends Model
{
    protected $table = "Evénement";

    public $timestamps = false;
    
    protected $dates = ['date'];

    protected $fillable=['titre', 'description', 'date', 'infoSupplémentaire'];
}
