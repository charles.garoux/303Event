<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inscription extends Model
{
    protected $table = "Inscription";

    public $timestamps = false;

    protected $fillable=['idEquipe', 'état', 'idEvénement', 'idJoueur'];
}
