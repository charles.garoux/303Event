<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = "Team";

    public $timestamps = false;

    protected $fillable=['nom', 'ville'];
}
