<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Joueur extends Model
{
    protected $table = "Joueur";

    public $timestamps = false;

    protected $fillable=['identifiant', 'motDePasse', 'nom', 'prénom', 'pseudo', 'idTeam', 'idHabitation', 'dateDeNaissance', 'numTel', 'email', 'numSécuritéSocial', 'cléContrôleSécuritéSocial', 'dateExpiration', 'cléDeRécupération'];
}
