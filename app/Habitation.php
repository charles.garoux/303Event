<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Habitation extends Model
{
    protected $table = "Habitation";

    public $timestamps = false;

    protected $fillable=['adresse', 'ville', 'codePostal'];
}