<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class AuthentificationAdministrateur
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Session::get('Staff')->rang == "Administrateur")
        {
            return $next($request);
        }
        Session::flash('erreur', "Cette fonctionnalité est accessible seulement aux administrateurs.");
        return redirect("administration/connexion");
    }
}
