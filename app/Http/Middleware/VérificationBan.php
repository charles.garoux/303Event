<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;
use App\Ban;

class VérificationBan
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!(Session::has('Joueur') && Ban::where('idJoueur', Session::get("Joueur")->id)->first()))
        {
            return $next($request);
        }
        return redirect(route('303Event.joueur.ban'));
    }
}
