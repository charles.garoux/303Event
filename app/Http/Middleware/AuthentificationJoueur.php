<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;
use App\Joueur;

class AuthentificationJoueur
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Session::has("Joueur") || Session::has("Staff"))
        {
            if(Session::has("Joueur"))
            {
                $dateExpiration = Joueur::find(Session::get('Joueur')->id)->dateExpiration;
                if(now() >= $dateExpiration)
                {
                    Session::flash('attention', "La date d'expiration de vos données est dépassée et ont donc été supprimé pour des raisons de sécurité de vos données. Vous devez les entrer de nouveau. Merci de votre compréhension.");
                    return redirect(route('303Event.joueur.réactualisation.formulaire'));
                }
            }
            return $next($request);
        }
        return redirect(route('303Event.joueur.connexion.formulaire'));
    }
}
