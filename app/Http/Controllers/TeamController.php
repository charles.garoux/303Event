<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Team;
use App\Joueur;
use App\Véhicule;


class TeamController extends Controller
{
    public function formulaireAjout(): object
    {
    	return view("inscriptionNouvelleTeam");
    }

    public function ajoutTeam(Request $request)
    {
    	$nom = $request->input('nom');
    	$ville = $request->input('ville');
        $Joueur = Session::get('Joueur');

        if(Team::where('nom', $nom)->first())
        {
            Session::flash('erreur', "Une team possède déjà ce nom.");
            return back();
        }

    	$Team = Team::create([
    		"nom" => $nom,
    		"ville" => $ville,
    	]);

    	if($Joueur)
    	{
    		DB::table('Joueur')->where('id', $Joueur->id)->update(['idTeam' => $Team->id]);
            $Joueur = Joueur::select('id','nom','prénom', 'idTeam')->where('id', $Joueur->id)->first();
            Session::put('Joueur', $Joueur);
            Session::flash('succès', "Vous avez ajouté votre team.");
            return redirect(route('303Event.accueil'));
    	}
        
        Session::flash('succès', "La team à été ajouté.");
    	return redirect(route('303Event.administration.team.liste'));
    }

    public function fiche()
    {
    	$Joueur = Session::get("Joueur");
    	$Team = Team::find($Joueur->idTeam);
        
    	if($Team == null || $Team->nom == "Freelance")
    	{
    		Session::flash('erreur', "Vous n'avez pas de team. Vous pouvez ajouter la votre ici:");
    		return redirect(route('303Event.team.nouvelle.formulaire'));
    	}
    	$Team->Membres = Joueur::select('nom', 'prénom')->where('idTeam', $Team->id)->get();
    	$Véhicules = Véhicule::select('id', 'nom', 'type', 'joueur')->where('idTeam', $Team->id)->get();
    	return view("ficheTeam", compact('Joueur', 'Team', 'Véhicules'));
    }

    public function ajoutNouveauVéhicule(Request $request)
    {
    	$nom = $request->input('nomVéhicule');
    	$type = $request->input('typeVéhicule');
    	$joueur = $request->input('véhiculeJoueur');
    	$idTeam = Session::get('Joueur')->idTeam;

    	if(!($nom && $type && $joueur))
    	{
    		Session::flash('erreur', "La fiche du véhicule n'est pas complète.");
    		return redirect(route('303Event.joueur.team.fiche'));
    	}
    	Véhicule::create([
    		'nom' => $nom,
    		'type' => $type,
    		'joueur' => $joueur,
    		'idTeam' => $idTeam
    	]);
    	Session::flash('succès', "Le véhicule a bien été ajouté.");
    	return redirect(route('303Event.joueur.team.fiche'));
    }

    public function ajoutSuppressionVéhicule($idVéhicule)
    {
    	$Véhicule = Véhicule::find($idVéhicule);
    	$Véhicule->delete();

    	Session::flash('attention', "Le véhicule a bien été supprimé.");
    	return redirect(route('303Event.joueur.team.fiche'));
    }

}
