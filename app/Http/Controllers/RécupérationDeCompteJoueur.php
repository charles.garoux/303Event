<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use App\Joueur;
use Mail;

class RécupérationDeCompteJoueur extends Controller
{
	public function demandeFormulaire()
	{
		return view('Récupération.demande');
	}

	public function traitementDemande(Request $request)
	{
		$email = $request->input('email');

		$Joueur = Joueur::where('email', $email)->first();
		if($Joueur == null)
		{
			Session::flash('erreur', "Votre adresse e-mail est incorrect ou vous n'avez pas enregistré votre adresse e-mail.");
			return back();
		}
		$Joueur->cléDeRécupération = $this->générationClé();
		$Joueur->save();

		$lienDeRécupération = route('303Event.joueur.récupération.récupération.formulaire', $Joueur->cléDeRécupération);

		$données = array('Joueur' => $Joueur , 'lienDeRécupération' => $lienDeRécupération);
		Mail::send('Récupération.email', $données, function($message) use($Joueur) {
		    $message->to($Joueur->email)
		            ->subject('Récupération de compte 303Event');
		});
		return view('Récupération.envoyé', compact('Joueur'));
    }

    public function récupérationFormulaire($cléDeRécupération)
    {
    	$Joueur = Joueur::select('nom', 'prénom')->where('cléDeRécupération', $cléDeRécupération)->first();
    	if($Joueur == null)
    	{
    		Session::flash('erreur', "La clé de récupération est incorrect.");
    		return redirect(route('303Event.joueur.récupération.demande.formulaire'));
    	}
    	return view('Récupération.nouveauMotDePasse', compact('Joueur'));
    }

    public function traitementRécupération(Request $request, $cléDeRécupération)
    {
    	$nouveauMotDePasse = $request->input('nouveauMotDePasse');

    	$Joueur = Joueur::where('cléDeRécupération', $cléDeRécupération)->first();
    	if($Joueur == null)
    	{
    		Session::flash('erreur', "La clé de récupération est incorrect.");
    		return redirect(route('303Event.joueur.récupération.demande.formulaire'));
    	}
    	$Joueur->motDePasse = Hash::make($nouveauMotDePasse);
    	$Joueur->cléDeRécupération = null;
    	$Joueur->save();
    	Session::flash('succès', "Votre mot de passe a bien été changé.");
    	return redirect(route('303Event.joueur.connexion.formulaire'));
    }

    public function générationClé($longueur = 60)
    {
	    $listeCaractères = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $longueurListeCaractères = strlen($listeCaractères);
	    $clé = '';
	    for ($i = 0; $i < $longueur; $i++) {
	        $clé .= $listeCaractères[rand(0, $longueurListeCaractères - 1)];
	    }
	    return $clé;
	}
}
