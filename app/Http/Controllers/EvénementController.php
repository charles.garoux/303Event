<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Evénement;
use App\Equipe;
use App\Regroupement;
use App\Joueur;
use App\Inscription;

class EvénementController extends Controller
{
    //Le controller s'appel "EvénementController" car "Evénement" est déjà utilisé par le model
    public function listeEvénements()
    {
        if(Session::has("Evénements"))
        {
            $Evénements = Session::get("Evénements");
        }
        else
        {
           $Evénements = Evénement::get();
           $Evénements->nombre = Evénement::count();
        }
    	return view("Evénement.listeEvénements", compact("Evénements"));
    }

    public function ficheEvénement($idEvénement)
    {

    	$Evénement = Evénement::findOrFail($idEvénement);
        $Joueur = Session::get('Joueur');

        $Joueur->inscription = Inscription::where('idJoueur', $Joueur->id)->where('idEvénement', $idEvénement)->first();

    	$Regroupements = Regroupement::where('idEvénement', $Evénement->id)->get();
    	$Equipes = DB::table('Regroupement')
    		->join('Equipe', 'Regroupement.idEquipe', '=', 'Equipe.id')
    		->where('idEvénement', '=', $Evénement->id)
    		->get();
        $Evénement->controller = $this;
       	return view("Evénement.ficheEvénement", compact("Evénement","Equipes", "Joueur"));
    }

    public function recherche(Request $request)
    {
        $attribut = $request->input('attribut');
        $ordre = $request->input('ordre');

        if($attribut == "lesProchains")
        {
            if($ordre == "croissant")
            {
                $Evénements = Evénement::where('date', '>', now())->orderBy('date', 'ASC')->get();
            }
            elseif($ordre == "décroissant")
            {
                $Evénements = Evénement::where('date', '>', now())->orderBy('date', 'DESC')->get();
            }
            else
            {
                $Evénements = Evénement::where('date', '>', now())->get();
            }
            $Evénements->nombre = Evénement::where('date', '>', now())->count();
            Session::flash('Evénements', $Evénements);
        }
        else
        {
            if($ordre == "croissant")
            {
                $Evénements = Evénement::orderBy('date', 'ASC')->get();
            }
            elseif($ordre == "décroissant")
            {
                $Evénements = Evénement::orderBy('date', 'DESC')->get();
            }
            else
            {
                Session::flash('erreur','Aucun attribut ou ordre séléctionné.');
                return redirect(route('303Event.événement.liste'));
            }
            $Evénements->nombre = Evénement::where('date', '>', now())->count();
            Session::flash('Evénements', $Evénements);
        }

    	return redirect(route('303Event.événement.liste'));
    }

    public function inscription(Request $request, $idEvénement)
    {
        if(!($idEquipe = $request->input('idEquipe')))
        {
            Session::flash('erreur', "Vous devez choisir une équipe.");
            return redirect()->route('303Event.événement.fiche',['idEvénement' => $idEvénement]);
        }

        $Regroupement = Regroupement::where('idEquipe', $idEquipe)->where('idEvénement', $idEvénement)->first();
        $Equipe = Equipe::find($idEquipe);

        if($this->nbrJoueurEquipe($idEquipe, $idEvénement) >= $Regroupement->nbJoueurMax && $Regroupement->nbJoueurMax > 0)
        {
            Session::flash('erreur', "Le nombre maximum joueur atteint dans l'équipe " . $Equipe->nom);
            return redirect()->route('303Event.événement.fiche',['idEvénement' => $idEvénement]);
        }
        
        $Joueur = Joueur::select('id', 'dateDeNaissance')->where('id', Session::get('Joueur')->id)->first();

        if($Regroupement->besoinAutorisation || now()->diffInYears($Joueur->dateDeNaissance) < 18)
        {
            Inscription::create([
                "idEquipe" => $Equipe->id,
                "état" => "en attente",
                "idEvénement" => $idEvénement,
                "idJoueur" => $Joueur->id
            ]);
        }
        else
        {
            Inscription::create([
                "idEquipe" => $Equipe->id,
                "état" => "accepté",
                "idEvénement" => $idEvénement,
                "idJoueur" => $Joueur->id
            ]);
        }
        Session::flash('succès', "Vous êtes inscrit dans l'équipe " . $Equipe->nom);
    	return redirect()->route('303Event.événement.fiche',['idEvénement' => $idEvénement]);
    }

    public function annulationInscription($idEvénement)
    {
        $Joueur = Session::get('Joueur');

        $Inscription = Inscription::where('idJoueur', $Joueur->id)->where('idEvénement', $idEvénement)->first();
        $Inscription->delete();

        Session::flash('attention', "Vous avez annulé votre inscription à cet événement.");
    	return redirect()->route('303Event.événement.fiche',['idEvénement' => $idEvénement]);
    }

    public function rechercheRegroupement($idEquipe, $idEvénement)
    {
        $Regroupement = Regroupement::where('idEvénement', $idEvénement)->where('idEquipe', $idEquipe)->first();
        return $Regroupement;
    }

    public function nbrJoueurEquipe($idEquipe, $idEvénement)
    {
        $nbJoueur = Inscription::where('idEquipe', $idEquipe)->where('idEvénement', $idEvénement)->count();
        return $nbJoueur;
    }
}
