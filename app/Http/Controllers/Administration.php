<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use App\Joueur;
use App\Habitation;
use App\Team;
use App\Ban;
use App\Note;
use App\Inscription;
use App\Staff;
use App\Véhicule;

class Administration extends Controller
{
	/////////// 303Event.administration.liste.joueurs.* ///////////

	public function listeJoueurs()
	{
		if(Session::get("listeJoueurs"))
		{
			$Joueurs = Session::get("listeJoueurs");
			$Joueurs->Controller = $this;
			return view("Administration.listeJoueurs", compact('Joueurs'));
		}
		$Joueurs = DB::table('Joueur')->get();
		$Joueurs->Controller = $this;
		$Joueurs->nombre = DB::table('Joueur')->count();
		return view("Administration.listeJoueurs", compact("Joueurs"));
	}

	public function recherche(Request $request)
	{
		$attribut = $request->input('attribut');
		$recherche = $request->input('recherche');
		$afficherBanni = $request->input('afficherBanni');

		if($attribut == null)
		{
			Session::flash('erreur','Aucun attribut séléctionné.');
			return redirect()->route('303Event.administration.liste.joueurs');
		}
		elseif($recherche == null)
		{
			Session::flash('erreur','Aucune recherche entré.');
			return redirect()->route('303Event.administration.liste.joueurs');
		}
		elseif($attribut == "team")
		{
			$idTeam = DB::table('Team')->select('id')->where('nom', '=', $recherche)->first();
			$idTeam = $idTeam->id;
			Session::flash('succès','Recherche effectué.');
			$Joueurs = DB::table('Joueur')->where('idTeam', '=', $idTeam)->get();
			$Joueurs->nombre = DB::table('Joueur')->where('idTeam', '=', $idTeam)->count();
			Session::flash('listeJoueurs', $Joueurs);
			return redirect()->route('303Event.administration.liste.joueurs');
		}
		else
		{
			Session::flash('succès','Recherche effectué.');
			$Joueurs = DB::table('Joueur')->where($attribut, '=', $recherche)->get();
			$Joueurs->nombre = DB::table('Joueur')->where($attribut, '=', $recherche)->count();
			Session::flash('listeJoueurs', $Joueurs);
			return redirect()->route('303Event.administration.liste.joueurs');
		}
	}

	public function suppressionJoueur(Request $request, int $idJoueur)
	{
		$Joueur = Joueur::find($idJoueur);
		$Habitation = Habitation::find($Joueur->idHabitation);
		$Ban = Ban::where('idJoueur', $idJoueur)->first();
		$Inscriptions = Inscription::where('idJoueur', $idJoueur)->get();
		$Note = Note::where('idJoueur', $idJoueur)->first();
		$motDePasse = $request->input('motDePasse');

        if (Session::get('Staff')->rang != "Administrateur")
        {
            Session::flash('erreur', "Seul un administrateur peu supprimer les données d'un joueur.");
            return redirect(route('303Event.administration.liste.joueurs'));
        }
        else
        {
            $motDePasseHashé = Staff::find(Session::get('Staff')->id)->motDePasse;
            if (!(Hash::check($motDePasse, $motDePasseHashé)))
            {
                Session::flash('erreur', "Le mot de passe est incorrect.");
            	return redirect(route('303Event.administration.liste.joueurs'));
            }
        }

		foreach ($Inscriptions as $Inscription) {
			$Inscription->delete();
		}
		if($Ban)
		{
			$Ban->delete();
		}
		if($Note)
		{
			$Note->delete();
		}
		$Joueur->delete();
		$Habitation->delete();

		Session::flash("succès", "Toute les données du joueurs " . $Joueur->nom . " " . $Joueur->prénom . " à été supprimé.");

		return redirect(route('303Event.administration.liste.joueurs'));
	}

	public function expirationJoueur(Request $request, int $idJoueur)
	{
		$Joueur = Joueur::find($idJoueur);
		$Habitation = Habitation::find($Joueur->idHabitation);
		$motDePasse = $request->input('motDePasse');

        if (Session::get('Staff')->rang != "Administrateur")
        {
            Session::flash('erreur', "Seul un administrateur peu supprimer les données d'un joueur.");
            return redirect(route('303Event.administration.liste.joueurs'));
        }
        else
        {
            $motDePasseHashé = Staff::find(Session::get('Staff')->id)->motDePasse;
            if (!(Hash::check($motDePasse, $motDePasseHashé)))
            {
                Session::flash('erreur', "Le mot de passe est incorrect.");
            	return redirect(route('303Event.administration.liste.joueurs'));
            }
        }

        if(now() < $Joueur->dateExpiration)
        {
			Session::flash('erreur', "Le mot de passe est incorrect.");
            return redirect(route('303Event.administration.liste.joueurs'));
        }

        $Joueur->nom = "XXXX";
        $Joueur->prénom = "XXXX";
        $Joueur->pseudo = null;
        $Joueur->idTeam = null;
        $Joueur->idHabitation = 0;
        $Joueur->dateDeNaissance = "9999-12-31";
        $Joueur->numTel = "XXXXXXXXXX";
        $Joueur->email = "XXXX@XXXX.xx";
        $Joueur->numSécuritéSocial = "XXXXXXXXXXXXX";
        $Joueur->cléContrôleSécuritéSocial = "XX";

        $Joueur->save();

		Session::flash("succès", "Les données du joueur ont bien été expiré. Le compte n'est pas supprimé, mais il devra les réactualiser.");

		return redirect(route('303Event.administration.liste.joueurs'));

	}

	/////////// 303Event.administration.banJoueur.* ///////////

	public function bannir(Request $request)
	{
		$raison = $request->input('raisonBannissement');
		$idJoueur = $request->input('idJoueur');
		$Joueur = Joueur::find($idJoueur);

		Ban::create([
    		"raison" => $raison,
    		"idJoueur" => $idJoueur
    	]);
		Session::flash('attention', "Vous venez de bannir ". $Joueur->nom . " " . $Joueur->prénom . "!");
		return redirect()->route('303Event.administration.liste.joueurs');
	}

	public function débannir(Request $request)
	{
		$idJoueur = $request->input('idJoueur');
		$Joueur = Joueur::find($idJoueur);
		$ban = Ban::where('idJoueur', $idJoueur)->first();

		$ban->delete();
		Session::flash("attention", $Joueur->nom . " " . $Joueur->prénom . " à été débanni!");
		return redirect()->route('303Event.administration.liste.joueurs');
	}

	/////////// 303Event.administration.notation.* ///////////

	public function noter(Request $request)
	{
		$idJoueur = $request->input('idJoueur');
		$noteRolePlay = $request->input('noteRolePlay');
		$notePonctualité = $request->input('notePonctualité');
		$noteFairPlay = $request->input('noteFairPlay');

		Note::create([
			"rolePlay" => $noteRolePlay,
			"ponctualité" => $notePonctualité,
			"fairPlay" => $noteFairPlay,
			"idJoueur" => $idJoueur
		]);
		return redirect()->route('303Event.administration.liste.joueurs');
	}

	public function actualiserNotes(Request $request)
	{
		$idJoueur =$request->input('idJoueur');
		$noteRolePlay = $request->input('noteRolePlay');
		$notePonctualité = $request->input('notePonctualité');
		$noteFairPlay = $request->input('noteFairPlay');

		$Notes = Note::where('idJoueur', $idJoueur)->first();
		$Notes->rolePlay = $noteRolePlay;
		$Notes->ponctualité = $notePonctualité;
		$Notes->fairPlay = $noteFairPlay;

		$Notes->save();
		return redirect()->route('303Event.administration.liste.joueurs');
	}

	public function note(int $idJoueur, string $typeNote)
	{
		if($Note = Note::where('idJoueur', $idJoueur)->first())
		{
			if ($typeNote == "Role-play")
			{
				return $Note->rolePlay;
			}
			elseif ($typeNote == "Ponctualité")
			{
				return $Note->ponctualité;
			}
			elseif ($typeNote == "Fair-play")
			{
				return $Note->fairPlay;
			}
		}
		return;
	}

	public function notes(int $idJoueur)
	{
		if($Notes = Note::where('idJoueur', $idJoueur)->first())
		{
			return ("Role-play: " . $Notes->rolePlay . " Ponctualité: " . $Notes->ponctualité . " Fair-play " . $Notes->fairPlay);
		}
		return;
	}

	public function moyenne(int $idJoueur)
	{
		if($Notes = Note::where('idJoueur', $idJoueur)->first())
		{
			$Notes->moyenne = round(($Notes->rolePlay + $Notes->ponctualité + $Notes->fairPlay)/3, 1);
		return $Notes->moyenne;
		}
		return;
	}

	public function listeTeams()
	{
		$Teams = Team::all();
		$Teams->nombre = Team::count();

		foreach ($Teams as $Team)
		{
			$Team->Membres = Joueur::select('id', 'nom', 'prénom', 'pseudo')->where('idTeam', $Team->id)->get();
			$Team->Membres->nombre = Joueur::where('idTeam', $Team->id)->count();
			$Team->Véhicules = Véhicule::select('id', 'nom', 'type', 'joueur')->where('idTeam', $Team->id)->get();
			$Team->Véhicules->nombre = Véhicule::where('idTeam', $Team->id)->count();
		}
		return view('Administration.listeTeams', compact('Teams'));
	}

	public function éjecterMembre($idJoueur)
	{
		$Joueur = Joueur::find($idJoueur);
		$Team = Team::select('nom')->where('id', $Joueur->idTeam)->first();
		$Joueur->idTeam = null;
		$Joueur->save();
		Session::flash('attention', "Vous avez éjecté " . $Joueur->nom . " " . $Joueur->prénom . " de la team: " . $Team->nom);
		return redirect(route('303Event.administration.team.liste'));
	}

	public function suppressionTeam(Request $request, $idTeam)
	{
		$motDePasse = $request->input('motDePasse');

        if (Session::get('Staff')->rang != "Administrateur")
        {
            Session::flash('erreur', "Seul un administrateur peu supprimer les données d'une team.");
            return redirect(route('303Event.administration.team.liste'));
        }
        else
        {
            $motDePasseHashé = Staff::find(Session::get('Staff')->id)->motDePasse;
            if (!(Hash::check($motDePasse, $motDePasseHashé)))
            {
                Session::flash('erreur', "Le mot de passe est incorrect.");
            	return redirect(route('303Event.administration.team.liste'));
            }
        }

        $Team = Team::find($idTeam);
        $Joueurs = Joueur::where('idTeam', $Team->id)->get();
        $Véhicules = Véhicule::where('idTeam', $Team->id)->get();
        
        foreach ($Joueurs as $Joueur)
        {
        	$Joueur->idTeam = null;
        	$Joueur->save();
        }
        foreach ($Véhicules as $Véhicule)
        {
        	$Véhicule->delete();
        }
        $Team->delete();
        
        Session::flash('attention', "Vous avez supprimer la team " . $Team->nom . " et tous ses véhicules.");
        return redirect(route('303Event.administration.team.liste'));
	}
}