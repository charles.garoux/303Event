<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use App\Joueur;
use App\Habitation;

class inscriptionNouveauJoueur extends Controller
{
    public function formulaire(): object
    {
    	$Teams = DB::table('Team')->select('id', 'nom')->get();
    	return view("inscriptionNouveauJoueur", compact('Teams'));
    }

    public function ajout(Request $request)
    {
    	$identifiant = $request->input('identifiant');
    	$motDePasse = $request->input('motDePasse');
    	$nom = $request->input('nom');
    	$prénom = $request->input('prénom');
    	$pseudo = $request->input('pseudo');
    	$idTeam = $request->input('idTeam');
    	$dateDeNaissance = $request->input('dateDeNaissance');
    	$email = $request->input('email');
    	$numTel = $request->input('numTel');
    	$numSécuritéSocial = $request->input('numSécuritéSocial');
    	$cléContrôleSécuritéSocial = $request->input('cléContrôleSécuritéSocial');
    	$adresse = $request->input('adresse');
    	$ville = $request->input('ville');
    	$codePostal = $request->input('codePostal');
    	$nouvelleTeam = $request->input('nouvelleTeam');
    	$conditionsUtilisation = $request->input('conditionsUtilisation');
    	$dateExpiration = now()->modify('+1 year');

        if( strlen((string)$numSécuritéSocial) != 13 || ((97-(($numSécuritéSocial)%97)) != $cléContrôleSécuritéSocial))
        {
            Session::flash('erreur', "Le numéro de sécurité social entré est incorrect.");
            return back();
        }

    	if($nouvelleTeam == "oui")
    	{
    		$idTeam = null;
    	}

    	$Habitation = Habitation::create([
    		"adresse" => $adresse,
    		"ville" => $ville,
    		"codePostal" => $codePostal,
    	]);

    	$Joueur = Joueur::create([
    		"identifiant" => $identifiant,
    		"motDePasse" => Hash::make($motDePasse),
    		"nom" => $nom,
    		"prénom" => $prénom,
    		"pseudo" => $pseudo,
    		"idTeam" => $idTeam,
    		"idHabitation" => $Habitation->id,
    		"dateDeNaissance" => $dateDeNaissance,
    		"numTel" => $numTel,
    		"email" => $email,
    		"numSécuritéSocial" => $numSécuritéSocial,
    		"cléContrôleSécuritéSocial" => $cléContrôleSécuritéSocial,
    		"dateExpiration" => $dateExpiration
    	]);

    	Session::flash('attention', "Les données précédemment enregistrées seront supprimés dans un an, soir approximativement le " . $dateExpiration->format('d-m-Y') . ".");
        Session::put("Joueur", $Joueur);

    	if($nouvelleTeam == "oui")
    	{
    		return redirect(route('303Event.team.nouvelle.formulaire', ['idJoueur' => $Joueur->id]));
    	}
    	return redirect(route('303Event.accueil'));
    }
}
