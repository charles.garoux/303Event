<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use App\Staff;

class AdministrationInterne extends Controller
{
    public function listeStaff()
	{
		$Staffs = DB::table('Staff')->where('rang', 'Modérateur')->get();
		$Staffs->Controller = $this;
		$Staffs->nombre = DB::table('Staff')->where('rang', 'Modérateur')->count();
		return view("Administration.listeStaff", compact("Staffs"));
	}

	public function suppressionStaff(int $idStaff)
	{
		$Staff = Staff::find($idStaff);
		if($Staff->rang == "Administrateur")
		{
			Session::flash('erreur', 'Un administrateur ne peut pas être supprimé.');
			return redirect(route('303Event.administration.liste.staff'));
		}
		$Staff->delete();

		Session::flash("succès", "Toute le staff " . $Staff->identifiant ." à été supprimé.");
		return redirect(route('303Event.administration.liste.staff'));
	}

	public function formulaireAjout()
	{
		return view('Administration.ajoutModérateur');
	}

	public function ajout(Request $request)
	{
		$identifiant = $request->input('identifiant');
		$motDePasse = $request->input('motDePasse');

		$Modérateur = Staff::create([
			'identifiant' => $identifiant,
			'motDePasse' => Hash::make($motDePasse),
			'rang' => "Modérateur"
		]);

		Session::flash('succès', "Le modérateur " . $Modérateur->identifiant . " à été ajouté.");
		return redirect(route('303Event.administration.liste.staff'));
	}
}
