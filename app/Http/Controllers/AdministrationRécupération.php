<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use App\Joueur;
use App\Staff;
use Mail;

class AdministrationRécupération extends Controller
{
    public function demandeFormulaireJoueur()
    {
    	return view('Administration.RécupérationJoueur.demande');
    }

    public function traitementDemandeJoueur(Request $request)
    {
    	$idJoueur = $request->input('idJoueur');
    	$email = $request->input('email');
    	$envoieEmail = $request->input('envoieEmail');
        $motDePasse = $request->input('motDePasse');

        if (Session::get('Staff')->rang != "Administrateur")
        {
            Session::flash('erreur', "Seul un administrateur peu faire la démarche de récupération de compte pour un joueur.");
            return redirect(route('303Event.administration.liste.joueurs'));
        }
        else
        {
            $motDePasseHashé = Staff::find(Session::get('Staff')->id)->motDePasse;
            if (!(Hash::check($motDePasse, $motDePasseHashé)))
            {
                Session::flash('erreur', "Le mot de passe est incorrect.");
                return back();
            }
        }

    	$Joueur = Joueur::find($idJoueur);
    	if($Joueur == null)
    	{
    		Session::flash('erreur', "Aucun joueur correspond à cet identifiant.");
    		return back();
    	}
    	$Joueur->cléDeRécupération = $this->générationClé();
		$Joueur->save();

		$lienDeRécupération = route('303Event.joueur.récupération.récupération.formulaire', $Joueur->cléDeRécupération);

    	if($envoieEmail == "oui")
    	{
			$données = array('Joueur' => $Joueur , 'lienDeRécupération' => $lienDeRécupération);
			Mail::send('Récupération.email', $données, function($message) use($email) {
			    $message->to($email)
			            ->subject('Récupération de compte 303Event');
			});
			return view('Administration.RécupérationJoueur.envoyé', compact('email'));
    	}
    	return view('Administration.RécupérationJoueur.lienRécupération', compact('lienDeRécupération', 'Joueur'));
    }

    public function générationClé($longueur = 60)
    {
	    $listeCaractères = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $longueurListeCaractères = strlen($listeCaractères);
	    $clé = '';
	    for ($i = 0; $i < $longueur; $i++) {
	        $clé .= $listeCaractères[rand(0, $longueurListeCaractères - 1)];
	    }
	    return $clé;
	}
}