<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Staff;

class ConnexionStaff extends Controller
{
    public function formulaire()
    {
    	return view("connexion");
    }

    public function vérification(Request $request)
    {
    	$identifiant = $request->input('identifiant');
    	$motDePasse = $request->input('motDePasse');

    	if(!($Staff = Staff::select('id')->where('identifiant', $identifiant)->first()))
    	{
    		Session::flash('erreur', "L'identifiant est incorrecte.");
    		return redirect(route('303Event.administration.connexion.formulaire'));
    	}

		$motDePasseHashé = Staff::find($Staff->id)->motDePasse;

    	if(Hash::check($motDePasse, $motDePasseHashé))
		{
			$Staff = Staff::select('id','rang')->where('identifiant', $identifiant)->first();
			Session::put('Staff', $Staff);
			Session::flash('succès', "Bonjour " . $Staff->rang);
			return redirect(route('303Event.accueil'));
		}
		Session::flash('erreur', "Le mot de passe est incorrecte.");
    	return redirect(route('303Event.administration.connexion.formulaire'));
    }

    public function déconnexion()
    {
    	Session::flush();
        return redirect(route('303Event.administration.connexion.formulaire'));
    }
}
