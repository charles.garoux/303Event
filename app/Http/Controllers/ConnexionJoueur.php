<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use App\Joueur;
use App\Ban;
use App\Team;
use App\Habitation;


class ConnexionJoueur extends Controller
{
    public function formulaire()
    {
    	return view("connexion");
    }

    public function vérification(Request $request)
    {
    	$identifiant = $request->input('identifiant');
    	$motDePasse = $request->input('motDePasse');

        if(!($Joueur = Joueur::select('id')->where('identifiant', $identifiant)->first()))
        {
            Session::flash('erreur', "L'identifiant est incorrecte.");
            return redirect(route('303Event.joueur.connexion.formulaire'));
        }

        $motDePasseHashé = Joueur::find($Joueur->id)->motDePasse;

    	if(Hash::check($motDePasse, $motDePasseHashé))
		{
			$Joueur = Joueur::select('id','nom','prénom', 'idTeam')->where('identifiant', $identifiant)->first();
			Session::put('Joueur', $Joueur);
			Session::flash('succès', "Bonjour " . $Joueur->nom . " " . $Joueur->prénom);
			return redirect(route('303Event.accueil'));
		}
        Session::flash('erreur', "Le mot de passe est incorrecte.");
        return redirect(route('303Event.joueur.connexion.formulaire'));
    }

    public function déconnexion()
    {
    	Session::flush();
        return redirect(route('303Event.joueur.connexion.formulaire'));
    }

    public function ban()
    {
        $Joueur = Joueur::find(Session::get('Joueur')->id);
        $Ban = Ban::where('idJoueur', $Joueur->id)->first();
        
        return view('banni', compact('Ban'));
    }

    public function réactualisationFormulaire()
    {
        $Teams = DB::table('Team')->select('id', 'nom')->get();
        return view("réactualisation", compact('Teams'));
    }

    public function réactualisationVérification(Request $request)
    {
        $identifiant = $request->input('identifiant');
        $motDePasse = $request->input('motDePasse');
        $nom = $request->input('nom');
        $prénom = $request->input('prénom');
        $pseudo = $request->input('pseudo');
        $idTeam = $request->input('idTeam');
        $dateDeNaissance = $request->input('dateDeNaissance');
        $email = $request->input('email');
        $numTel = $request->input('numTel');
        $numSécuritéSocial = $request->input('numSécuritéSocial');
        $cléContrôleSécuritéSocial = $request->input('cléContrôleSécuritéSocial');
        $adresse = $request->input('adresse');
        $ville = $request->input('ville');
        $codePostal = $request->input('codePostal');
        $nouvelleTeam = $request->input('nouvelleTeam');
        $conditionsUtilisation = $request->input('conditionsUtilisation');
        $dateExpiration = now()->modify('+1 year');

        if($nouvelleTeam == "oui")
        {
            $idTeam = null;
        }

        $Joueur = Joueur::find(Session::get('Joueur')->id);

        $Habitation = Habitation::create([
            "adresse" => $adresse,
            "ville" => $ville,
            "codePostal" => $codePostal,
        ]);

        $Joueur->identifiant = $identifiant;
        $Joueur->motDePasse = Hash::make($motDePasse);
        $Joueur->nom = $nom;
        $Joueur->prénom = $prénom;
        $Joueur->pseudo = $pseudo;
        $Joueur->idTeam = $idTeam;
        $Joueur->idHabitation = $Habitation->id;
        $Joueur->dateDeNaissance = $dateDeNaissance;
        $Joueur->numTel = $numTel;
        $Joueur->email = $email;
        $Joueur->numSécuritéSocial = $numSécuritéSocial;
        $Joueur->cléContrôleSécuritéSocial = $cléContrôleSécuritéSocial;
        $Joueur->dateExpiration = $dateExpiration;
        
        $Joueur->save();

        $Joueur = Joueur::select('id','nom','prénom', 'idTeam')->where('id', $Joueur->id)->first();
        Session::put('Joueur', $Joueur);

        Session::flash('attention', "Les données précédemment enregistrées seront supprimés dans un an, soit approximativement le " . $dateExpiration->format('d-m-Y') . ".");

        if($nouvelleTeam == "oui")
        {
            return redirect(route('303Event.team.nouvelle.formulaire', ['idJoueur' => $Joueur->id]));
        }
        return redirect(route('303Event.accueil'));
    }
}
