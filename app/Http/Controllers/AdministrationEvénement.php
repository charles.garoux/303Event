<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Evénement;
use App\Equipe;
use App\Regroupement;
use App\Inscription;
use App\Staff;


class AdministrationEvénement extends Controller
{
    public function formulaireCréation()
    {
    	$Equipes = Equipe::get();
    	return view("Administration.créationEvénément", compact("Equipes"));
    }

    public function créationEvénement(Request $request)
    {
    	$titre = $request->input('titre');
    	$date = $request->input('date');
    	$description = $request->input('description');
        $infoSupplémentaire = $request->input('infoSupplémentaire');

    	$Evénement = Evénement::create([
    		"titre" => $titre,
    		"date" => $date,
    		"description" => $description,
            "infoSupplémentaire" => $infoSupplémentaire
    	]);

		$Equipes = Equipe::get();
    	foreach ($Equipes as $Equipe)
        {
            if(($nbJoueurMax = $request->input('nbJoueurMaxEquipe' . $Equipe->id)) == null)
            {
                $nbJoueurMax = 0;
            }
    		if($request->input('Equipe' . $Equipe->id))
    		{
                if($request->input('besoinAutorisation' . $Equipe->id))
                {
                        Regroupement::create([
                        "idEvénement" => $Evénement->id,
                        "idEquipe" => $Equipe->id,
                        "nbJoueurMax" => $nbJoueurMax,
                        'besoinAutorisation' => true
                    ]);
                }
                else
                {
                   Regroupement::create([
                        "idEvénement" => $Evénement->id,
                        "idEquipe" => $Equipe->id,
                        "nbJoueurMax" => $nbJoueurMax
                    ]); 
                }
    		}
    	}
    	Session::flash('succès', "L'événement à été créé.");

    	return redirect(route('303Event.administration.événement.fiche', ['idEvénement' => $Evénement->id]));
    }

    public function ficheEvénement($idEvénement)
    {
        $Evénement = Evénement::find($idEvénement);

        $Regroupements = Regroupement::where('idEvénement', $Evénement->id)->get();
        $Equipes = DB::table('Regroupement')
            ->join('Equipe', 'Regroupement.idEquipe', '=', 'Equipe.id')
            ->where('idEvénement', '=', $Evénement->id)
            ->get();
        $Evénement->controller = $this;

        $Joueurs = DB::table('Inscription')
            ->select('Joueur.*')
            ->join('Joueur', 'Inscription.idJoueur', '=', 'Joueur.id')
            ->where('idEvénement', $Evénement->id)->get();

        foreach ($Joueurs as $Joueur)
        {
            $Joueur->Inscription = DB::table('Inscription')
            ->select('Inscription.*')
            ->join('Joueur', 'Inscription.idJoueur', '=', 'Joueur.id')
            ->where('idJoueur', $Joueur->id)
            ->where('idEvénement', $Evénement->id)->first();

            $Joueur->Equipe = Equipe::find($Joueur->Inscription->idEquipe);
        }

        return view("Administration.ficheEvénement", compact("Evénement","Equipes", "Joueurs"));
    }

    public function changerEtatInscription(Request $request)
    {
        $idInscription = $request->input('idInscription');
        $nouvelEtatInscription = $request->input('nouvelEtatInscription');
        $motDePasse = $request->input('motDePasse');

        $Inscription = Inscription::find($idInscription);

        if($Inscription->état == "réglé")
        {
            if (Session::get('Staff')->rang != "Administrateur")
            {
                Session::flash('erreur', "Quand une inscription est réglé elle ne peut pas changer d'état pour éviter tous problème. Si sont état une erreur, contacter un administrateur.");
                return redirect(route('303Event.administration.événement.fiche', ['idEvénement' => $Inscription->idEvénement]));
            }
            else
            {
                $motDePasseHashé = Staff::find(Session::get('Staff')->id)->motDePasse;
                if (!(Hash::check($motDePasse, $motDePasseHashé)))
                {
                    Session::flash('erreur', "Le mot de passe est incorrect.");
                return redirect(route('303Event.administration.événement.fiche', ['idEvénement' => $Inscription->idEvénement]));
                }
            }
        }

        $Inscription->état = $nouvelEtatInscription;
        $Inscription->save();

        return redirect(route('303Event.administration.événement.fiche', ['idEvénement' => $Inscription->idEvénement]));
    }

    public function suppressionEvénement(Request $request, $idEvénement)
    {
        $Evénement = Evénement::find($idEvénement);
        $Regroupements = Regroupement::where('idEvénement', $idEvénement)->get();
        $Inscriptions = Inscription::where('idEvénement', $idEvénement)->get();
        $motDePasse = $request->input('motDePasse');

        if (Session::get('Staff')->rang != "Administrateur")
        {
            Session::flash('erreur', "Quand une inscription est réglé elle ne peut pas changer d'état pour éviter tous problème. Si sont état une erreur, contacter un administrateur.");
            return redirect(route('303Event.événement.liste'));
        }
        else
        {
            $motDePasseHashé = Staff::find(Session::get('Staff')->id)->motDePasse;
            if (!(Hash::check($motDePasse, $motDePasseHashé)))
            {
                Session::flash('erreur', "Le mot de passe est incorrect.");
                return redirect(route('303Event.événement.liste'));
            }
        }

        foreach ($Regroupements as $Regroupement) {
            $Regroupement->delete();
        }
        foreach ($Inscriptions as $Inscription) {
            $Inscription->delete();
        }
        $Evénement->delete();

        Session::flash("succès", "Toute les données de l'événement " . $Evénement->titre . " à été supprimé.");

        return redirect(route('303Event.événement.liste'));
    }

    public function listeEquipes()
    {
        $Equipes = Equipe::all();
        $Equipes->nombre = Equipe::count();
        
        return view('Administration.listeEquipes', compact('Equipes'));
    }

    public function suppressionEquipe($idEquipe)
    {
        $Equipe = Equipe::find($idEquipe);
        $Equipe->delete();

        Session::flash('attention', "Vous avez supprimé l'équipe " . $Equipe->nom);
        return redirect(route('303Event.administration.événement.équipe.liste'));
    }

    public function formulaireAjoutEquipe()
    {
        return view('Administration.ajoutEquipe');
    }

    public function ajoutEquipe(Request $request)
    {
        $nom = $request->input('nom');
        $description = $request->input('description');

        $Equipe = Equipe::create([
            "nom" => $nom,
            "description" => $description,
        ]);
        
        Session::flash('succès', "L'équipe " . $Equipe->nom . " à été ajouté.");
        return redirect(route('303Event.administration.événement.équipe.liste'));
    }

    public function rechercheRegroupement($idEquipe, $idEvénement)
    {
        $Regroupement = Regroupement::where('idEvénement', $idEvénement)->where('idEquipe', $idEquipe)->first();
        return $Regroupement;
    }

    public function nbrJoueurEquipe($idEquipe, $idEvénement)
    {
        $nbJoueur = Inscription::where('idEquipe', $idEquipe)->where('idEvénement', $idEvénement)->count();
        return $nbJoueur;
    }
}
