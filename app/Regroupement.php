<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Regroupement extends Model
{
    protected $table = "Regroupement";

    public $timestamps = false;

    protected $fillable=['idEvénement', 'idEquipe', 'nbJoueur', 'nbJoueurMax', 'besoinAutorisation'];
}