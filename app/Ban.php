<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ban extends Model
{
    protected $table = "Ban";

    public $timestamps = false;

    protected $fillable=['raison', 'idJoueur'];
}
