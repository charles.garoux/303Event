<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipe extends Model
{
    protected $table = "Equipe";

    public $timestamps = false;

    protected $fillable=['nom', 'description'];
}
