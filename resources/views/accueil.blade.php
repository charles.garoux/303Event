@extends('includes.template+navbar')
@section('entete')
<h1>303 Airsoft Team</h1>
@endsection
@section('contenu')
<div class="row justify-content-center">
	<div id="carouselExampleIndicators" class="carousel slide my-3" data-ride="carousel">
		<ol class="carousel-indicators">
			<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
			<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
		</ol>
		<div class="carousel-inner">
			<div class="carousel-item active">
				<img class="d-block w-100" src="https://scontent-cdg2-1.xx.fbcdn.net/v/t1.0-9/33418085_2114109401934149_5768880903824605184_n.jpg?_nc_cat=0&oh=28e31671125de2a7a3ef92f51b008546&oe=5B86AA4C" alt="Première slide">
				<div class="carousel-caption d-none d-md-block">
					<h5>Zone safe</h5>
					<p>Ceci est là zone safe de notre terrain où l'on pose nos répliques le temps de faire une pause et de rigoler sans différence d'équipe.</p>
				</div>
			</div>
			<div class="carousel-item">
				<img class="d-block w-100" src="https://scontent-cdg2-1.xx.fbcdn.net/v/t1.0-9/33328872_2112422162102873_3675984722493702144_n.jpg?_nc_cat=0&oh=a8552b5e5da2258a3ceb76368a0716a9&oe=5BAFB1DC" alt="Deuxième slide">
				<div class="carousel-caption d-none d-md-block">
					<h5>Notre véhicule d'OP</h5>
					<p>Ceci est le véhicule joueur qu'on utilise en OP.</p>
				</div>
			</div>
		</div>
		<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Précédent</span>
		</a>
		<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Suivant</span>
		</a>
	</div>
</div>
<div class="row">
	<div class="col-xl-6 col-md-4 col-sm-12 col-12">
		<div class="card shadow-lg mb-5 bg-tan rounded">
			<div class="card-header">
				Présentation de l'association
			</div>
			<div class="card-body">
				<p>L’association 303 Airsoft team regroupe des passionné partageant l'esprit de l’airsoft autant sur notre terrain que sur celui des autres.</p>
			</div>
		</div>
	</div>
	<div class="col-xl-3 col-md-4 col-sm-12 col-12">
		<div class="card shadow-lg mb-5 bg-tan rounded">
			<div class="card-header">
				Informations de contacte
			</div>
			<div class="card-body">
				<ul>
					<li>07 81 91 15 98</li>
					<li><a href="https://www.facebook.com/303airsoftteam">Facebook</a></li>
				</ul>
			</div>
		</div>
		<div class="card shadow-lg mb-5 bg-tan rounded">
			<div class="card-header">
				Informations sur l'association
			</div>
			<div class="card-body">
				<p>Association à but non lucratif créer en 2010</p>
			</div>
		</div>
	</div>
	<div class="col-xl-3 col-md-4 col-sm-12 col-12">
		<div class="card shadow-lg mb-5 bg-tan rounded">
			<div class="card-header">
				Informations sur notre terrain
			</div>
			<div class="card-body">
				<p>Situé à Saint-Sauveur-en-Rue en foret sur <span class="text-danger">X</span> hectare</p>
			</div>
		</div>
	</div>
</div>
@endsection