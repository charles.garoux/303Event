@extends('includes.template+navbar')
@section('entete')
<h3>Inscription d'une nouvelle team</h3>
@endsection
@section('contenu')
<br>
<form class="row" method="post">
	@csrf
	<div class="col-xl-3 col-md-12 col-sm-12 col-12">
		<div class="card shadow-lg mb-5 bg-tan rounded">
			<div class="card-header">
				Informations sur la team
			</div>
			<div class="card-body">
				<div class="form-group row">
					<label for="nom" class="col-sm-3 col-form-label" >Nom*:</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="nom" id="nom" maxlength="32" required/>
					</div>
				</div>
				<div class="form-group row">
					<label for="ville" class="col-sm-3 col-form-label" >Ville*:</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="ville" id="ville" maxlength="32" required/>
					</div>
				</div>
				@if(Session::has('Joueur'))
					<button type="submit" class="btn btn-primary btn-block">Enregistrer ma team</button>
				@else
					<button type="submit" class="btn btn-primary btn-block">Enregistrer cette nouvelle team</button>
				@endif
			</div>
		</div>
	</div>
</form>
@endsection