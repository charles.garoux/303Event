@extends('includes.template')
@section('entete')
<h3 class="text-danger">Vous avez été banni</h3>
<a class="btn btn-danger" href="{{ route('303Event.joueur.déconnexion')}}">Se déconnecter</a>
@endsection
@section('contenu')
<div class="col-xl-6 col-md-12 col-sm-12 col-12">
		<div class="card shadow-lg mb-5 bg-tan rounded">
			<div class="card-header">
				Raison du ban
			</div>
			<div class="card-body">
				{{$Ban->raison}}
			</div>
		</div>
	</div>
@endsection