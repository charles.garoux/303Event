@extends('includes.template')
@section('entete')
<h3>Informations sur les données personnelles collecté par l'application</h3>
@endsection
@section('contenu')
<div class="col-12">
	<div class="card shadow-lg mb-5 bg-tan rounded">
		<div class="card-header">
			<span class="text-info">Pourquoi toutes ces informations sont demandées?</span>
		</div>
		<div class="card-body row">
			<table class="table table-hover table-responsive">
				<thead>
					<th scope="col">Données personnelles</th>
					<th scope="col">Obligatoire</th>
					<th scope="col">Raison</th>
					<th scope="col">Conséquence d'un défaut de réponse</th>
				</thead>
				<tbody>
					<th scope="row">
						Nom et prénom
					</th>
					<td>
						Oui
					</td>
					<td>
						Votre nom et prénom nous de vous identifier lors de nos événements.
					</td>
					<td>
						Si l'on ne peu pas vous identifier, pour des raison de sécurité les événements vous seront interdit.
					</td>
				</tbody>
				<tbody>
					<th scope="row">
						Date de naissance
					</th>
					<td>
						Oui
					</td>
					<td>
						Votre date de naissance nous permet d'identifier les mineurs et appliquer des règle d'inscription spécifique pour eux.
					</td>
					<td>
						Si la date de naissance est incorrecte et que vous êtes mineurs, pour des raison de sécurité les événements vous seront interdit.
					</td>
				</tbody>
				<tbody>
					<th scope="row">
						Numéro de sécurité social (NIR)
					</th>
					<td>
						Oui
					</td>
					<td>
						Votre NIR permet, en cas d'accidents lors d'un de nos événement, de le fournir au secours et aux assurances.
					</td>
					<td>
						Si le NIR est incorrecte il ne pourrai pas être fournie au secours en cas d'accident ni aux assurances et donc ralentir le travail secours.
					</td>
				</tbody>
				<tbody>
					<th scope="row">
						La clé de contrôle
					</th>
					<td>
						Oui
					</td>
					<td>
						Cette clé permet de vérifier si le NIR entré est correct.
					</td>
					<td>
						Si la clé est incorrecte le NIR le sera aussi et donc aura les même conséquences que le défaut de réponse pour le NIR.
					</td>
				</tbody>
				<tbody>
					<th scope="row">
						Adresse e-mail
					</th>
					<td>
						Non
					</td>
					<td>
						Votre adresse e-mail pourrai permettre de vous contacter pour vous avertir de certaines informations en rapport avec les événements organisé et ceux auquel vous vous êtes inscrit(un nouvel événement, annulation d'un événement où vous êtes inscrit, modification d'un événement où vous êtes inscrit).
					</td>
					<td>
						Si l'adresse e-mail est incorrecte aucun e-mail pourrai vous être envoyé et si l'adresse est à une autres personnes elle serai importuné par des e-mail non désiré.
					</td>
				</tbody>
				<tbody>
					<th scope="row">
						Numéro de téléphone
					</th>
					<td>
						Non
					</td>
					<td>
						Votre numéro de téléphone pourrai permettre de vous contacter pour vous avertir de certaines informations en rapport avec les événements organisé et ceux auquel vous vous êtes inscrit(un nouvel événement, annulation d'un événement où vous êtes inscrit, modification d'un événement où vous êtes inscrit).
					</td>
					<td>
						Si l'adresse e-mail est incorrecte aucun e-mail pourrai vous être envoyé et si l'adresse est à une autres personnes elle serai importuné par des e-mail non désiré.
					</td>
				</tbody>
				<tbody>
					<th scope="row">
						Lieu d'habitation
					</th>
					<td>
						Oui
					</td>
					<td>
						
					</td>
					<td>
						
					</td>
				</tbody>
				<tbody>
					<th scope="row">
						Pseudo
					</th>
					<td>
						Non
					</td>
					<td>
						Votre pseudo pourrai permettre de vous reconnaître plus facilement lors d'événements.
					</td>
					<td>
						Si votre pseudo est incorrecte il sera plus compliqué de vous reconnaître.
					</td>
				</tbody>
				<tbody>
					<th scope="row">
						Team d'airsoft
					</th>
					<td>
						Non
					</td>
					<td>
						Votre team pourrai permettre de vous reconnaître et vous regrouper plus facilement lors d'événements.
					</td>
					<td>
						Si la team enregistré n'est pas la votre cela pourrai gêner les membres de cette team.
					</td>
				</tbody>
				<tbody>
					<th scope="row">
						Identifiant et mot de passe
					</th>
					<td>
						Oui
					</td>
					<td>
						L'identifiant et le mot de passe vous permettront de vous connecter à l'application.
					</td>
					<td>
						Il ne peu pas avoir de défaut de réponse pour ces données là.
					</td>
				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="col-12">
	<div class="card shadow-lg mb-5 bg-tan rounded">
		<div class="card-header">
			<span class="text-info">Le ou les destinataires de vos données personnelles</span>
		</div>
		<div class="card-body">
			<p>Vos données personnelles ne seront jamais vendu à une organisation tiers.</p>
			<p>Vos données personnelles pourront être fournis aux services d'urgence et à des assurances seulement en cas d'accident sur l'un des événements.</p>
		</div>
	</div>
</div>
<div class="col-12">
	<div class="card shadow-lg mb-5 bg-tan rounded">
		<div class="card-header">
			<span class="text-info">Les cookies</span>
		</div>
		<div class="card-body">
			<p>Les seul cookies utilisé sont là pour le fonctionnement de l'application et pour sa sécurisation</p>
			<table class="table table-hover">
				<thead>
					<th scope="col">Cookie</th>
					<th scope="col">Utilité</th>
				</thead>
				<tbody>
					<th scope="row">
						laravel_session
					</th>
					<td>
						Enregistre la "session" de la personne connecté, elle permet de garder en mémoire des données utilisé sur plusieurs partie de l'application.<br>
						La "session" permet aussi d'afficher des "flash"(un "flash" est un message d’avertissement en général et qui s'affichera qu'une fois. Si la page est actualisé ou changé le flash sera supprimé).
						Exemple: nom et prénom pour l'afficher à l'écran d'accueil après sa connexion.
					</td>
				</tbody>
				<tbody>
					<th scope="row">
						XSRF-TOKEN
					</th>
					<td>
						Ce cookie est utilisé pour protéger des attaques CSRF (ou XSRF).
					</td>
				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="col-12">
	<div class="card shadow-lg mb-5 bg-tan rounded">
		<div class="card-header">
			<span class="text-info">Vos droits</span>
		</div>
		<div class="card-body">
			<p>En cas de besoin vous pouvez contacter le responsable pour pouvoir consulter toutes vos données, les modifier, les supprimer ou les faire anonymiser.</p>
		</div>
	</div>
</div>
<div class="col-12">
	<div class="card shadow-lg mb-5 bg-tan rounded">
		<div class="card-header">
			<span class="text-info">Responsable et informations de contacte</span>
		</div>
		<div class="card-body">
			<p>Yann Faverjon est le responsable et peu être contacté par téléphone et par e-mail à ces coordonnées:</p>
			<ul>
				<li>XXXXXXXXXX</li>
				<li>XXXXX@XXXXX</li>
			</ul>
		</div>
	</div>
</div>
@endsection