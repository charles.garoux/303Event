@extends('includes.template+navbar')
@section('entete')
<h1>{{$Evénement->titre}}</h1>
@endsection
@section('contenu')
<div class="row">
	<div class="col-xl-6 col-md-12 col-sm-12 col-12">
		<div class="card shadow-lg mb-5 bg-tan rounded">
			<div class="card-header">
				Informations sur l'événement
			</div>
			<div class="card-body">
				<?php echo($Evénement->description); ?>
			</div>
		</div>
	</div>
	<div class="col-xl-3 col-md-12 col-sm-12 col-12">
		<div class="col-12">
			<div class="card shadow-lg mb-5 bg-tan rounded">
				<div class="card-header">
					Informations sur les équipes
				</div>
				<div class="card-body">
					<table>
						@foreach($Equipes as $Equipe)
						<tr>
							<th>{{ $Equipe->nom }}</th>
							<td>{{ $Equipe->description }}</td>
						</tr>
						@endforeach()
					</table>
				</div>
			</div>
		</div>
		<div class="col-12">
			<div class="card shadow-lg mb-5 bg-tan rounded">
				<div class="card-header">
					Informations supplémentaire
				</div>
				<div class="card-body">
					<p><strong>Date: {{ $Evénement->date->format('d-m-Y') }}</strong></p>
					<p>
						<?php echo($Evénement->infoSupplémentaire); ?>
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xl-3 col-md-12 col-sm-12 col-12">
		<div class="card shadow-lg mb-5 bg-tan rounded">
			<div class="card-header">
				Informations d'inscription
			</div>
			<div class="card-body">
				@if($Evénement->date > now())
				@if(!($Joueur->inscription))
				<form action="{{ route('303Event.événement.inscription',['idEvénement' => $Evénement->id]) }}" method="post">
					@csrf
					<table class="table">
						<thead>
							<tr>
								<th scope="col"></th>
								<th scope="col">Nom</th>
								<th scope="col">Places</th>
							</tr>
						</thead>
						<tbody>
							@foreach($Equipes as $Equipe)
							<tr>
								<th scope="row">
									@if ($Evénement->controller->nbrJoueurEquipe($Equipe->id,$Evénement->id) < $Evénement->controller->rechercheRegroupement($Equipe->id,$Evénement->id)->nbJoueurMax || $Evénement->controller->rechercheRegroupement($Equipe->id,$Evénement->id)->nbJoueurMax == 0)
									<input class="form-check-input" type="radio" value="{{$Equipe->id}}" id="Equipe{{$Equipe->id}}" name="idEquipe" value="{{$Equipe->id}}">
									@else
									<input class="form-check-input" type="radio" value="{{$Equipe->id}}" id="Equipe{{$Equipe->id}}" name="idEquipe" value="{{$Equipe->id}}" disabled>
									@endif
								</th>
								<td>
									<label class="form-check-label" for="Equipe{{$Equipe->id}}">
										{{$Equipe->nom}}
									</label>
								</td>
								<td>
									@if ($Evénement->controller->rechercheRegroupement($Equipe->id,$Evénement->id)->nbJoueurMax <= 0)
									<label class="form-check-label" for="Equipe{{$Equipe->id}}">
										{{$Evénement->controller->nbrJoueurEquipe($Equipe->id,$Evénement->id)}}/&#8734;
									</label>
									@else
									<label class="form-check-label" for="Equipe{{$Equipe->id}}">
										{{$Evénement->controller->nbrJoueurEquipe($Equipe->id,$Evénement->id)}}/{{$Evénement->controller->rechercheRegroupement($Equipe->id,$Evénement->id)->nbJoueurMax}}
									</label>
									@endif
								</td>
							</tr>
							@endforeach()
						</tbody>
					</table>
					<button type="submit" class="btn btn-primary">M'inscrire</button>
				</form>
				@elseif($Joueur->inscription)
				<table class="table">
					<thead>
						<tr>
							<th scope="col">Nom</th>
							<th scope="col">Places</th>
						</tr>
					</thead>
					<tbody>
						@foreach($Equipes as $Equipe)
							<tr>
								<td>
									<label class="form-check-label" for="Equipe{{$Equipe->id}}">
										{{$Equipe->nom}}
									</label>
								</td>
								<td>
									@if ($Evénement->controller->rechercheRegroupement($Equipe->id,$Evénement->id)->nbJoueurMax <= 0)
									<label class="form-check-label" for="Equipe{{$Equipe->id}}">
										{{$Evénement->controller->nbrJoueurEquipe($Equipe->id,$Evénement->id)}}/&#8734;
									</label>
									@else
									<label class="form-check-label" for="Equipe{{$Equipe->id}}">
										{{$Evénement->controller->nbrJoueurEquipe($Equipe->id,$Evénement->id)}}/{{$Evénement->controller->rechercheRegroupement($Equipe->id,$Evénement->id)->nbJoueurMax}}
									</label>
									@endif
								</td>
							</tr>
							@endforeach()
					</tbody>
				</table>
				<p>Déjà inscrit, votre demande est <strong>{{ $Joueur->inscription->état }}</strong>.</p>
				<form action="{{ route('303Event.événement.inscription.annulation',['idEvénement' => $Evénement->id]) }}" method="post">
					@csrf
					<button type="submit" class="btn btn-danger">Annuler mon inscription</button>
				</form>
				@endif
				@else
					<table class="table">
						<thead>
							<tr>
								<th scope="col">Nom</th>
								<th scope="col">Places</th>
							</tr>
						</thead>
						<tbody>
							@foreach($Equipes as $Equipe)
							<tr>
								<th scope="row">

									<label class="form-check-label" for="Equipe{{$Equipe->id}}">
										{{$Equipe->nom}}
									</label>
								</th>
								<td>
									@if ($Evénement->controller->rechercheRegroupement($Equipe->id,$Evénement->id)->nbJoueurMax <= 0)
									<label class="form-check-label" for="Equipe{{$Equipe->id}}">
										{{$Evénement->controller->nbrJoueurEquipe($Equipe->id,$Evénement->id)}}/&#8734;
									</label>
									@else
									<label class="form-check-label" for="Equipe{{$Equipe->id}}">
										{{$Evénement->controller->nbrJoueurEquipe($Equipe->id,$Evénement->id)}}/{{$Evénement->controller->rechercheRegroupement($Equipe->id,$Evénement->id)->nbJoueurMax}}
									</label>
									@endif
								</td>
							</tr>
							@endforeach()
						</tbody>
					</table>
				@endif
			</div>
		</div>
	</div>
</div>
@endsection