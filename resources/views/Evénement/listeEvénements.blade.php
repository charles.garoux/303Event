@extends('includes.template+navbar')
@section('entete')
<h1>Liste des événements</h1>
@if(Session::has('Staff'))
<a class="btn btn-success row float-sm-right float-none my-sm-0 mb-2" href="{{ route('303Event.administration.événement.formulaireCréation') }}">Ajouter un événement</a>
@endif
<form class="row" action="{{ route("303Event.événement.liste.recherche") }}" method="post">
	@csrf
	<div class="form-inline">
		<select class="custom-select mr-sm-2" name="attribut">
			<option value="" selected>Attribut recherché</option>
			<option value="lesProchains" >Les prochains événements</option>
		</select>
		<div class="form-check form-check-inline">
			<input class="form-check-input" type="radio" name="ordre" id="ordreCroissant" value="croissant">
			<label class="form-check-label" for="ordreCroissant">
				Du ancien au plus tard
			</label>
		</div>
		<div class="form-check form-check-inline">
			<input class="form-check-input" type="radio" name="ordre" id="ordreDécroissant" value="décroissant">
			<label class="form-check-label" for="ordreDécroissant">
				Du plus tard au plus ancien
			</label>
		</div>
		<button type="submit" class="btn btn-primary mx-sm-2 mt-sm-0 mt-2">Lancer la recherche</button>
	</div>
</form>
@endsection
@section("barre.latérale.gauche")
<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
<span>Informations supplémentaires</span>
</h6>
<li class="nav-item">
	<p class="ml-5 mt-1">
		Nombre total d'événements: {{ $Evénements->nombre }}
	</p>
</li>
@endsection
@section("contenu")
<table class="table table-hover table-responsive-sm bg-tan mt-3">
	<thead>
		<tr>
			<th scope="col">#</th>
			<th scope="col">Titre</th>
			<th scope="col">Date</th>
			<th scope="col">Option</th>
		</tr>
	</thead>
	<tbody>
		@foreach($Evénements as $Evénement)
		@if(Session::has('Joueur') && DB::table('Inscription')->where('idJoueur', Session::get('Joueur')->id)->where('idEvénement', $Evénement->id)->first()) {{-- Le joueur est inscrit à cet événement --}}
		<tr class="bg-info">
		@elseif($Evénement->date > now()) {{-- l'événement est à venir --}}
		<tr>
		@else {{-- L'évenement est aujourd'hui ou est passé --}} 
			<tr class="table-dark">
		@endif
				<th scope="row">{{ $Evénement->id }}</th>
				<td>{{ $Evénement->titre }}</td>
				<td>{{ $Evénement->date->format('d-m-Y') }}</td>
				<td>
					@if(Session::has('Staff'))
					<a class="btn btn-primary" href="{{ route('303Event.administration.événement.fiche', ['idEvénement' => $Evénement->id]) }}" role="button">Fiche</a>
					@if(Session::get('Staff')->rang == 'Administrateur')
					<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#Suppression{{$Evénement->id}}">
					Supprimer
					</button>
					<div class="modal fade text-body" id="Suppression{{$Evénement->id}}" tabindex="-1" role="dialog">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<form action="{{ route('303Event.administration.événement.suppression', $Evénement->id) }}" method="POST">
									@csrf
									{{ method_field("delete") }}
									<div class="modal-header">
										<h5 class="modal-title">Suppression d'événement</h5>
									</div>
									<div class="modal-body">
										<p>Êtes-vous sûr de vouloir supprimer l'événement <strong>{{ $Evénement->titre . " du " . $Evénement->date->format('d-m-Y') }}</strong> ?</p>
										<div class="form-group pt-3">
											<label for="motDePasse">Mot de passe:</label>
											<input type="password" class="form-control" id="motDePasse" name="motDePasse">
										</div>
									</div>
									<div class="modal-footer">
										
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
										<button class="btn btn-danger" type="submit">Supprimer</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					@endif
					@else
					<a class="btn btn-primary" href="{{ route('303Event.événement.fiche', ['idEvénement' => $Evénement->id]) }}" role="button">Fiche</a>
					@endif
				</td>
			</tr>
			@endforeach()
		</tbody>
	</table>
	@endsection