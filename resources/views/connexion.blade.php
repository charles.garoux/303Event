@extends('includes.template')
@section('entete')
<h3>Connection à 303Event</h3>
@endsection
@section('contenu')
<div class="row justify-content-center">
    <div class="col-xl-4 col-md-6 col-sm-12 col-12">
        <div class="card shadow-lg mb-5 bg-tan rounded">
            <div class="card-header text-center">
                Informations de connexion
            </div>
            <div class="card-body">
                <form action="" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="identifiant"><strong>Identifiant:</strong></label>
                        <input type="text" class="form-control" id="identifiant" name="identifiant" required>
                    </div>
                    <div class="form-group">
                        <label for="motDePasse"><strong>Mot de passe:</strong></label>
                        <input type="password" class="form-control" id="motDePasse" name="motDePasse" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Me connecter</button>
                    @if(Route::is('303Event.joueur.connexion.formulaire'))
                    <a class="btn btn-secondary float-right" href="{{ route('303Event.joueur.nouveau.formulaire') }}" role="button">M'inscrire</a>
                    <div class="row">
                        <div class="col-12">
                            <a class="btn btn-outline-warning btn-sm mt-3" href="{{ route('303Event.joueur.récupération.demande.formulaire') }}" role="button">Récupération de compte</a>
                        </div>
                        
                    </div>
                    @endif
                </form>
            </div>
        </div>
    </div>
</div>
@endsection