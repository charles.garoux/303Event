<!doctype html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset("css/airsoft.css") }}">
    <title>303Event</title>
  </head>
  <body class="bg-multicam">
    <div class="row">
      <main role="main" class="pt-3 px-3 col-12">
        <div class="container-fluid">
          <div class="card bg-tan px-3 mb-3">
            <div class="card-body">
              @yield('entete')
            </div>
          </div>
          @include('includes.flash')
          @yield('contenu')
        </div>
      </main>
    </div>
    @if(Route::is('303Event.joueur.connexion.formulaire') || Route::is('303Event.administration.connexion.formulaire'))
    <footer class="row justify-content-center fixed-bottom">
      @include('includes.piedDePage')
    </footer>
    @elseif(Route::is('303Event.joueur.récupération.demande.formulaire') ||  Route::is('303Event.joueur.récupération.demande.traitement') ||  Route::is('303Event.joueur.récupération.récupération.formulaire'))
    <footer class="row justify-content-center fixed-bottom footer">
      @include('includes.piedDePage')
    </footer>
    @else
    <footer class="row justify-content-center relative-bottom footer">
      @include('includes.piedDePage')
    </footer>
    @endif

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
  </body>
</html>