<!doctype html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset("css/airsoft.css") }}">
    <link rel="stylesheet" href="{{ asset("css/sidebar.css") }}">
    <title>303Event</title>
  </head>
  <body class="bg-multicam">
    <nav class="navbar navbar-expand-lg navbar-light bg-tan">
      <a class="navbar-brand h1" href="{{ route('303Event.accueil')}}">303Event</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          @if (Session::has('Staff'))
          {{-- Bar de navigation du staff --}}
          @if (Route::is('303Event.administration.liste.joueurs') || Route::is('303Event.administration.liste.joueurs.recherche'))
          <li class="nav-item active">
            <a class="nav-link" href="{{ route('303Event.administration.liste.joueurs') }}">Liste joueurs</a>
          </li>
          @else
          <li class="nav-item">
            <a class="nav-link" href="{{ route('303Event.administration.liste.joueurs') }}">Liste joueurs</a>
          </li>
          @endif
          @if (Route::is('303Event.administration.team.liste'))
          <li class="nav-item active">
            <a class="nav-link" href="{{ route('303Event.administration.team.liste') }}">Liste teams</a>
          </li>
          @else
          <li class="nav-item">
            <a class="nav-link" href="{{ route('303Event.administration.team.liste') }}">Liste teams</a>
          </li>
          @endif
          @if (Route::is('303Event.administration.événement.équipe.liste'))
          <li class="nav-item active">
            <a class="nav-link" href="{{ route('303Event.administration.événement.équipe.liste') }}">Liste équipes</a>
          </li>
          @else
          <li class="nav-item">
            <a class="nav-link" href="{{ route('303Event.administration.événement.équipe.liste') }}">Liste équipes</a>
          </li>
          @endif
          
          @if(Session::get('Staff')->rang == "Administrateur")
          {{-- Bar de navigation administrateur --}}
          @if (Route::is('303Event.administration.liste.staff'))
          <li class="nav-item active">
            <a class="nav-link" href="{{ route('303Event.administration.liste.staff') }}">Liste staff</a>
          </li>
          @else
          <li class="nav-item">
            <a class="nav-link" href="{{ route('303Event.administration.liste.staff') }}">Liste staff</a>
          </li>
          @endif
          @endif
          @endif
          @if (Session::has('Joueur'))
          {{-- Bar de navigation des joueurs --}}
          @if (Route::is('303Event.joueur.team.fiche'))
          <li class="nav-item active">
            <a class="nav-link" href="{{ route('303Event.joueur.team.fiche') }}">Ma team</a>
          </li>
          @else
          <li class="nav-item">
            <a class="nav-link" href="{{ route('303Event.joueur.team.fiche') }}">Ma team</a>
          </li>
          @endif
          @endif
          {{-- Bar de navigation commune --}}
          @if (Route::is('303Event.événement.liste'))
          <li class="nav-item active">
            <a class="nav-link" href="{{ route('303Event.événement.liste') }}">Liste événements</a>
          </li>
          @else
          <li class="nav-item">
            <a class="nav-link" href="{{ route('303Event.événement.liste') }}">Liste événements</a>
          </li>
          @endif
        </ul>
        <div class="my-2 my-lg-0">
          @if(Session::has("Joueur"))
          <a class="btn btn-danger" href="{{ route('303Event.joueur.déconnexion')}}">Se déconnecter</a>
          @elseif(Session::has("Staff"))
          <a class="btn btn-danger" href="{{ route('303Event.administration.déconnexion')}}">Se déconnecter</a>
          @endif
        </div>
      </div>
    </nav>
    <div class="container-fluid">
      <div class="row">
        @hasSection('barre.latérale.gauche')
        <nav class="col-md-2 d-none d-md-block bg-tan sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              @yield("barre.latérale.gauche")
            </ul>
          </div>
        </nav>
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-3">
          @else
          <main role="main" class="pt-3 px-3 col-12">
            @endif
            <div class="card bg-tan px-3 mb-3">
              <div class="card-body">
                @yield('entete')
              </div>
            </div>
            @include('includes.flash')
            @yield('contenu')
          </main>
        </div>
        @if(Route::is('303Event.administration.joueur.récupération.demande.formulaire') || Route::is('303Event.team.nouvelle.formulaire')) {{-- Le pied de page doit prendre moins de place sûr ces page car il peu gêner les utilisateur de smartphone --}}
        <footer class="row justify-content-center fixed-bottom">
          @include('includes.piedDePage')
        </footer>
        @elseif(Route::is('303Event.joueur.team.fiche'))
        <footer class="row justify-content-center fixed-bottom footer">
          @include('includes.piedDePage')
        </footer>
        @else
        <footer class="row justify-content-center relative-bottom footer">
          @include('includes.piedDePage')
        </footer>
        @endif
      </div>
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    </body>
  </html>