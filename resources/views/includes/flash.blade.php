@if (Session::has("succès"))
    <div class="alert alert-success" role="alert">
        <li>{{ Session::get("succès") }}</li>
    </div>
@endif
@if (Session::has("erreur"))
    <div class="alert alert-danger" role="alert">
        <li>{{ Session::get("erreur") }}</li>
    </div>
@endif
@if (Session::has("information"))
    <div class="alert alert-primary" role="alert">
        <li>{{ Session::get("information") }}</li>
    </div>
@endif
@if (Session::has("attention"))
    <div class="alert alert-warning" role="alert">
        <li>{{ Session::get("attention") }}</li>
    </div>
@endif