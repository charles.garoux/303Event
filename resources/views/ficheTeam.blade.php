@extends('includes.template+navbar')
@section('entete')
<h1>Ma team: {{ $Team->nom }}</h1>
@endsection
@section('contenu')
<div class="row">
	<div class="col-xl-4 col-lg-6 col-12">
		<div class="card shadow-lg mb-5 bg-tan rounded">
			<div class="card-header">
				Membres de la team
			</div>
			<div class="card-body">
				<table class="table">
					<thead>
						<th scope="col">Nom</th>
						<th scope="col">Prénom</th>
					</thead>
					<tbody>
						@foreach($Team->Membres as $Membre)
						<tr>
							<td>{{$Membre->nom}}</td>
							<td>{{$Membre->prénom}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-xl-4 col-lg-6 col-12">
		<div class="card shadow-lg mb-5 bg-tan rounded">
			<div class="card-header">
				Informations véhicule<small>(s)</small>
				<button type="button" class="btn btn-success float-right" data-toggle="modal" data-target="#ajoutVéhicule">
				Ajouter une véhicule
				</button>
			</div>
			<div class="card-body">
				<table class="table">
					<thead>
						<th scope="col">Nom</th>
						<th scope="col">Type</th>
						<th scope="col">Joueur</th>
						<th scope="col"></th>
					</thead>
					<tbody>
						@foreach($Véhicules as $Véhicule)
						<tr>
							<td>{{$Véhicule->nom}}</td>
							<td>{{$Véhicule->type}}</td>
							<td>{{$Véhicule->joueur}}</td>
							<td>
								<form action="{{ route('303Event.joueur.team.véhicule.suppression', $Véhicule->id) }}" method="post">
									@csrf
									{{ method_field("delete") }}
									<button  type="submit" class="btn btn-danger" >Supprimer</button>
								</form>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-xl-4 col-lg-6 col-12">
		<div class="card shadow-lg mb-5 bg-tan rounded">
			<div class="card-header">
				Informations sur les équipes
			</div>
			<div class="card-body">
				<p><strong>Origine:</strong> {{ $Team->ville }}</p>
			</div>
		</div>
	</div>
</div>
{{-- Modal d'ajout de véhicule --}}
<div class="modal fade" id="ajoutVéhicule" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content bg-tan">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">Fiche du nouveau véhicule</h5>
			</div>
			<form action="{{ route('303Event.joueur.team.véhicule.ajout') }}" method="post">
				@csrf
				<div class="modal-body">
					<div class="form-group">
						<label for="nomVéhicule">Nom du véhicule:</label>
						<input type="text" class="form-control" id="nomVéhicule" name="nomVéhicule" placeholder="exemple: Peugeot 406" required>
					</div>
					<div class="form-group">
						<label for="typeVéhicule">Type du véhicule</label>
						<select class="form-control" id="typeVéhicule" name="typeVéhicule" required>
							<option value="Voiture">Voiture</option>
							<option value="Moto">Moto</option>
							<option value="SUV">SUV</option>
							<option value="Pick-up">Pick-up</option>
							<option value="4x4">4x4</option>
							<option value="Camionnette">Camionnette</option>
							<option value="Camion">Camion</option>
						</select>
					</div>
					<div class="form-check form-check-inline">
						<input class="form-check-input" type="radio" name="véhiculeJoueur" id="véhiculeJoueur" value="Oui"  required>
						<label class="form-check-label" for="véhiculeJoueur">
							Véhicule joueur
						</label>
					</div>
					<div class="form-check form-check-inline">
						<input class="form-check-input" type="radio" name="véhiculeJoueur" id="véhiculeNonJoueur" value="Non" required checked>
						<label class="form-check-label" for="véhiculeNonJoueur">
							Véhicule non joueur
						</label>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">annuler</button>
					<button type="submit" class="btn btn-primary">Enregistrer ce véhicule</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection