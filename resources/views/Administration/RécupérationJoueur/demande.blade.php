@extends('includes.template+navbar')
@section('entete')
<h3>Demande de récupération de compte d'un joueur</h3>
@endsection
@section('contenu')
<div class="row justify-content-center">
    <div class="col-xl-4 col-md-6 col-sm-12 col-12">
        <div class="card shadow-lg mb-5 bg-tan rounded">
            <div class="card-header text-center">
                Informations de demande de récupération
            </div>
            <div class="card-body">
                <form action="" method="post">
                    @csrf
                    <div class="form-group">
                        <div class="row">
                            <label for="nom" class="col-sm-6 col-form-label" for="idJoueur"><strong>L'identifiant unique du joueur:</strong></label>
                            <div class="col-sm-6">
                                <input type="number" class="form-control" name="idJoueur" id="idJoueur"/>
                            </div>
                        </div>
                        <small>L'identifiant unique(#) se trouve dans la première colonne dans la liste des joueurs.</small>
                    </div>
                    <div class="form-group">
                        <label for="email">L'adresse e-mail où il souhaite recevoir le lien:</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <input type="checkbox" name="envoieEmail" value="oui">
                                </div>
                            </div>
                            <input type="email" class="form-control" id="email" name="email" >
                        </div>
                        <small>Cocher la case si le lien doit être envoyé par e-mail sinon le contenue du formulaire sera ignoré.</small>
                    </div>
                    <div class="form-group">
                        <label for="motDePasse"><strong>Mot de passe:</strong></label>
                        <input type="password" class="form-control" id="motDePasse" name="motDePasse" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Envoyer la requête</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection