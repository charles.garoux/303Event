@extends('includes.template+navbar')
@section('entete')
<h3>Récupération de compte</h3>
@endsection
@section('contenu')
<div class="row justify-content-center">
    <div class="col-xl-4 col-md-6 col-sm-12 col-12">
        <div class="card shadow-lg mb-5 bg-tan rounded">
            <div class="card-header text-center">
                <span class="text-success">Lien de récupération de compte de <strong>{{ $Joueur->nom . " " . $Joueur->prénom }}</strong></span>
            </div>
            <div class="card-body">
                <p class="text-danger"><strong>Ce lien doit être fournit au joueur à qui appartient le compte et à personne d'autre ! Le lien ne doit pas être non plus utilisé par une autre personne qu'elle !</strong></p>
                <input type="text" class="form-control" value="{{$lienDeRécupération}}" onclick="this.select()" readonly>
            </div>
        </div>
    </div>
</div>
@endsection