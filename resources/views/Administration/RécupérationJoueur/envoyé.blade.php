@extends('includes.template+navbar')
@section('entete')
<h3>Récupération de compte</h3>
@endsection
@section('contenu')
<div class="row justify-content-center">
    <div class="col-xl-4 col-md-6 col-sm-12 col-12">
        <div class="card shadow-lg mb-5 bg-tan rounded">
            <div class="card-header text-center">
                <span class="text-success">L'e-mail à bien été envoyé à <strong>{{ $email }}</strong></span>
            </div>
            <div class="card-body">
                <p>Le joueur va recevoir un e-mail comportant un lien pour récupérer votre compte commençant par "{{ route('303Event.joueur.récupération.récupération.formulaire', '') }}" et envoyé par "<strong>{{ env('MAIL_FROM_NAME', env('MAIL_USERNAME', "L'e-mail n'est pas configuré")) }}</strong>".</p>
            </div>
        </div>
    </div>
</div>
@endsection