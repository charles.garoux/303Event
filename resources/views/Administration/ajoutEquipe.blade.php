@extends('includes.template+navbar')
@section('entete')
<h3>Ajout d'une nouvelle équipe</h3>
@endsection
@section('contenu')
<br>
<form class="row" method="post">
	@csrf
	<div class="col-xl-4 col-md-12 col-sm-12 col-12">
		<div class="card shadow-lg mb-5 bg-tan rounded">
			<div class="card-header">
				Informations de l'équipe
			</div>
			<div class="card-body">
				<div class="form-group row">
					<label for="nom" class="col-sm-3 col-form-label" ><strong>Nom:</strong></label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="nom" id="nom" maxlength="32" required/>
					</div>
				</div>
				<div class="form-group row">
					<label for="description" class="col-sm-3 col-form-label" ><strong>Description:</strong></label>
					<div class="col-sm-9">
						<textarea type="text" class="form-control" name="description" id="description" maxlength="32" rows="5" required></textarea>
					</div>
				</div>
					<button type="submit" class="btn btn-primary btn-block">Enregistrer cette nouvelle équipe</button>
			</div>
		</div>
	</div>
</form>
@endsection