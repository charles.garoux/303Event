@extends('includes.template+navbar')
@section('entete')
<h3>Ajout d'un nouveau modérateur</h3>
@endsection
@section('contenu')
<form class="row" method="post">
	@csrf
	<div class="col-xl-4 col-md-6 col-sm-12 col-12 ">
		<div class="card shadow-lg mb-5 bg-tan rounded">
			<div class="card-header">
				Informations du modérateur
			</div>
			<div class="card-body">
				<div class="form-group row">
					<label for="identifiant" class="col-sm-3 col-form-label" >Identifiant*:</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="identifiant" id="identifiant" maxlength="32" required/>
					</div>
				</div>
				<div class="form-group row">
					<label for="motDePasse" class="col-sm-3 col-form-label" >Mot de passe*:</label>
					<div class="col-sm-9">
						<input type="password" class="form-control" name="motDePasse" id="motDePasse" maxlength="32" required/>
					</div>
				</div>
				<button type="submit" class="btn btn-primary btn-block">Enregistrer cette nouvelle team</button>
			</div>
		</div>
	</div>
</form>
@endsection