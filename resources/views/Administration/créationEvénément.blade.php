@extends('includes.template+navbar')
@section('entete')
<h3>Création d'un nouvel événement</h3>
@endsection
@section('contenu')
<form class="row" method="post">
	@csrf
		<div class="col-md-6 col-12">
			<div class="card shadow-lg p-3 mb-5 bg-tan rounded">
				<div class="card-header">
					Informations général
				</div>
				<div class="card-body">
					<div class="form-group row">
						<label for="titre" class="col-sm-4 col-form-label" >Titre*:</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" name="titre" id="titre" maxlength="32" required/>
						</div>
					</div>
					<div class="form-group row">
						<label for="date" class="col-sm-3 col-form-label" >Date*:</label>
						<div class="col-sm-9">
							<input type="date" class="form-control" name="date" id="date" required/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-4">
							<label for="description" class="col-form-label" >Description de l'événement<br>(Role-play si il y a)</label>
							<p class="text-danger"><small>Ecrire <strong class="">"&lt;br&gt;"</strong> pour les retour à la ligne sinon les retour à la ligne ne seront pas pris en compte.</small></p>
						</div>
						<div class="col-sm-8">
							<textarea class="form-control" id="description" name="description" rows="6"></textarea>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-4">
							<label for="infoSupplémentaire" class="col-form-label" >Information supplementaire (PAF, horaire, ...)</label>
							<p class="text-danger"><small>Ecrire <strong class="">"&lt;br&gt;"</strong> pour les retour à la ligne sinon les retour à la ligne ne seront pas pris en compte.</small></p>
						</div>
						<div class="col-sm-8">
							<textarea class="form-control" id="infoSupplémentaire" name="infoSupplémentaire" rows="4"></textarea>
						</div>
					</div>
					<button type="submit" class="btn btn-primary btn-block">Enregistrer cette événement</button>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-12">
			<div class="card shadow-lg p-3 mb-5 bg-tan rounded">
				<div class="card-header">
					Les équipes de l'événement
				</div>
				<div class="card-body table-responsive">					
					<table class="table">
						<thead>
							<tr>
								<th scope="col">Selection</th>
								<th scope="col">Nombre de joueur max</th>
								<th scope="col">Nom</th>
								<th scope="col">Description</th>
								<th scope="col">Besoin d'un autorisation</th>
							</tr>
						</thead>
						<tbody>
							@foreach($Equipes as $Equipe)
							<tr>
								<th scope="row">
									<input type="checkbox" value="{{$Equipe->id}}" id="Equipe{{$Equipe->id}}" name="Equipe{{$Equipe->id}}">
								</th>
								<td>
									<input type="number" class="form-control" name="nbJoueurMaxEquipe{{$Equipe->id}}">
								</td>
								<td>
									<label class="form-check-label" for="Equipe{{$Equipe->id}}">
										{{$Equipe->nom}}
									</label>
								</td>
								<td class="">
									<label class="form-check-label" for="Equipe{{$Equipe->id}}" style="word-break: keep-all">
										{{$Equipe->description}}
									</label>
								</td>
								<td class="justify-content-center">
									<input type="checkbox" value="{{$Equipe->id}}" id="besoinAutorisation{{$Equipe->id}}" name="besoinAutorisation{{$Equipe->id}}">
								</td>
							</tr>
							@endforeach()
						</tbody>
					</table>
				</div>
			</div>
		</div>
</form>
@endsection