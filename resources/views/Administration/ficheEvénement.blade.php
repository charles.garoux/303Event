@extends('includes.template+navbar')
@section('entete')
<h1>{{$Evénement->titre}}</h1>
@endsection
@section('contenu')
<div class="row">
	<div class="col-xl-6 col-md-12 col-sm-12 col-12">
		<div class="card shadow-lg mb-5 bg-tan rounded">
			<div class="card-header">
				Informations sur l'événement
			</div>
			<div class="card-body">
				<?php echo($Evénement->description); ?>
			</div>
		</div>
	</div>
	<div class="col-xl-3 col-md-12 col-sm-12 col-12">
		<div class="col-12">
			<div class="card shadow-lg mb-5 bg-tan rounded">
				<div class="card-header">
					Informations sur les équipes
				</div>
				<div class="card-body">
					<table>
						@foreach($Equipes as $Equipe)
						<tr>
							<th>{{ $Equipe->nom }}</th>
							<td>{{ $Equipe->description }}</td>
						</tr>
						@endforeach()
					</table>
				</div>
			</div>
		</div>
		<div class="col-12">
			<div class="card shadow-lg mb-5 bg-tan rounded">
				<div class="card-header">
					Informations supplémentaire
				</div>
				<div class="card-body">
					<p><strong>Date: {{ $Evénement->date->format('d-m-Y') }}</strong></p>
					<p>
						<?php echo($Evénement->infoSupplémentaire); ?>
					</p>
				</div>
			</div>
		</div>
		
	</div>
	<div class="col-xl-3 col-md-12 col-sm-12 col-12">
		<div class="card shadow-lg mb-5 bg-tan rounded">
			<div class="card-header">
				Informations d'inscription
			</div>
			<div class="card-body">
				<table class="table">
					<thead>
						<tr>
							<th scope="col">Nom</th>
							<th scope="col">Places</th>
						</tr>
					</thead>
					<tbody>
						@foreach($Equipes as $Equipe)
						<tr>
							<th scope="row">
								<label class="form-check-label" for="Equipe{{$Equipe->id}}">
									{{$Equipe->nom}}
								</label>
							</th>
							<td>
								@if ($Evénement->controller->rechercheRegroupement($Equipe->id,$Evénement->id)->nbJoueurMax <= 0)
								<label class="form-check-label" for="Equipe{{$Equipe->id}}">
									{{$Evénement->controller->nbrJoueurEquipe($Equipe->id,$Evénement->id)}}/&#8734;
								</label>
								@else
								<label class="form-check-label" for="Equipe{{$Equipe->id}}">
									{{$Evénement->controller->nbrJoueurEquipe($Equipe->id,$Evénement->id)}}/{{$Evénement->controller->rechercheRegroupement($Equipe->id,$Evénement->id)->nbJoueurMax}}
								</label>
								@endif
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<table class="table table-hover bg-tan">
		<thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">Nom</th>
				<th scope="col">Prénom</th>
				<th scope="col">Date de naissance</th>
				<th scope="col">Pseudo</th>
				<th scope="col">Team</th>
				<th scope="col">Adresse complète</th>
				<th scope="col">Numéro de téléphone</th>
				<th scope="col">Adresse Email</th>
				<th scope="col">Numéro de sécurité social (NIR)</th>
				<th scope="col">Clé de contrôle</th>
				<th scope="col">Equipe</th>
				<th scope="col">Etat</th>
				<th scope="col">Option</th>
			</tr>
		</thead>
		<tbody>
			@foreach($Joueurs as $Joueur)
			@if(now()->diffInYears($Joueur->dateDeNaissance) < 18) {{-- Joueur mineur--}}
				<tr class="bg-warning">
			@else
				<tr>
			@endif
					<th scope="row">{{ $Joueur->id }}</th>
					<td>{{ $Joueur->nom }}</td>
					<td>{{ $Joueur->prénom }}</td>
					<td>{{ date('d-m-Y',strtotime($Joueur->dateDeNaissance)) }}</td>
					<td>{{ $Joueur->pseudo }}</td>
					@if($Joueur->idTeam)
					<td>{{ DB::table('Team')->where('id', $Joueur->idTeam)->first()->nom }}</td>
					@else
					<td></td>
					@endif
					<td>
						{{ DB::table('Habitation')->where('id', $Joueur->idHabitation)->first()->adresse . " " . DB::table('Habitation')->where('id', $Joueur->idHabitation)->first()->ville . " " . DB::table('Habitation')->where('id', $Joueur->idHabitation)->first()->codePostal}}
					</td>
					<td>{{ $Joueur->numTel }}</td>
					<td>{{ $Joueur->email }}</td>
					<td>{{ $Joueur->numSécuritéSocial }}</td>
					<td>{{ $Joueur->cléContrôleSécuritéSocial }}</td>
					<td>{{ $Joueur->Equipe->nom }}</td>
					<td>{{ $Joueur->Inscription->état }}</td>
					<td>
						@if($Joueur->Inscription->état == "refusé")
						<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#état{{$Joueur->id}}">Refusé</button>
						<div class="modal fade" id="état{{$Joueur->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<form action="{{ route('303Event.administration.événement.inscription.changer.etat') }}" method="post">
										@csrf
										<input type="hidden" name="idInscription" value="{{$Joueur->Inscription->id}}">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalCenterTitle">Changer l'état de l’inscription</h5>
										</div>
										<div class="modal-body">
											<div class="form-group">
												<label for="nouvelEtatInscription">Changer l'état de l'inscription de {{ $Joueur->nom . " " . $Joueur->prénom }} :</label>
												<select class="form-control" id="nouvelEtatInscription" name="nouvelEtatInscription">
													<option value="accepté">Accepter</option>
													<option value="en attente">Mettre en attente</option>
													<option value="réglé">Réglé</option>
													<option value="absent">Absent</option>
												</select>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
											<button type="submit" class="btn btn-primary">Enregistrer le nouvel état</button>
										</div>
									</form>
								</div>
							</div>
						</div>
						@elseif($Joueur->Inscription->état == "en attente")
						<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#état{{$Joueur->id}}">En attente</button>
						<div class="modal fade" id="état{{$Joueur->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<form action="{{ route('303Event.administration.événement.inscription.changer.etat') }}" method="post">
										@csrf
										<input type="hidden" name="idInscription" value="{{$Joueur->Inscription->id}}">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalCenterTitle">Changer l'état de l’inscription</h5>
										</div>
										<div class="modal-body">
											<div class="form-group">
												<label for="nouvelEtatInscription">Changer l'état de l'inscription de {{ $Joueur->nom . " " . $Joueur->prénom }} :</label>
												<select class="form-control" id="nouvelEtatInscription" name="nouvelEtatInscription">
													<option value="refusé">Refuser</option>
													<option value="accepté">Accepter</option>
													<option value="réglé">Réglé</option>
													<option value="absent">Absent</option>
												</select>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
											<button type="submit" class="btn btn-primary">Enregistrer le nouvel état</button>
										</div>
									</form>
								</div>
							</div>
						</div>
						@elseif($Joueur->Inscription->état == "accepté")
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#état{{$Joueur->id}}">Accepté</button>
						<div class="modal fade" id="état{{$Joueur->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<form action="{{ route('303Event.administration.événement.inscription.changer.etat') }}" method="post">
										@csrf
										<input type="hidden" name="idInscription" value="{{$Joueur->Inscription->id}}">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalCenterTitle">Changer l'état de l’inscription</h5>
										</div>
										<div class="modal-body">
											<div class="form-group">
												<label for="nouvelEtatInscription">Changer l'état de l'inscription de {{ $Joueur->nom . " " . $Joueur->prénom }} :</label>
												<select class="form-control" id="nouvelEtatInscription" name="nouvelEtatInscription">
													<option value="refusé">Refuser</option>
													<option value="en attente">Mettre en attente</option>
													<option value="réglé">Réglé</option>
													<option value="absent">Absent</option>
												</select>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
											<button type="submit" class="btn btn-primary">Enregistrer le nouvel état</button>
										</div>
									</form>
								</div>
							</div>
						</div>
						@elseif($Joueur->Inscription->état == "réglé")
						<button type="button" class="btn btn-success" data-toggle="modal" data-target="#état{{$Joueur->id}}">Réglé</button>
						<div class="modal fade" id="état{{$Joueur->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalCenterTitle">Changer l'état de l’inscription</h5>
									</div>
									@if(Session::get('Staff')->rang == "Administrateur")
									<form action="{{ route('303Event.administration.événement.inscription.changer.etat') }}" method="post">
										@csrf
										<input type="hidden" name="idInscription" value="{{$Joueur->Inscription->id}}">
										<div class="modal-body">
											<div class="form-group">
												<label for="nouvelEtatInscription">Changer l'état de l'inscription de {{ $Joueur->nom . " " . $Joueur->prénom }} :</label>
												<select class="form-control" id="nouvelEtatInscription" name="nouvelEtatInscription">
													<option value="refusé">Refuser</option>
													<option value="accepté">Accepter</option>
													<option value="en attente">Mettre en attente</option>
													<option value="absent">Absent</option>
												</select>
												<div class="form-group pt-3">
													<label for="motDePasse">Mot de passe:</label>
													<input type="password" class="form-control" id="motDePasse" name="motDePasse">
												</div>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
											<button type="submit" class="btn btn-primary">Enregistrer le nouvel état</button>
										</div>
									</form>
									@else
									<div class="modal-body">
										Quand une inscription est "réglé" elle ne peut pas changer d'état pour éviter toute problème.<br>
										Si c'est une erreur, contacter un administrateur.
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
									</div>
									@endif
								</div>
							</div>
						</div>
						@elseif($Joueur->Inscription->état == "absent")
						<button type="button" class="btn btn-dark" data-toggle="modal" data-target="#état{{$Joueur->id}}">Absent</button>
						<div class="modal fade" id="état{{$Joueur->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<form action="{{ route('303Event.administration.événement.inscription.changer.etat') }}" method="post">
										@csrf
										<input type="hidden" name="idInscription" value="{{$Joueur->Inscription->id}}">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalCenterTitle">Changer l'état de l’inscription</h5>
										</div>
										<div class="modal-body">
											<div class="form-group">
												<label for="nouvelEtatInscription">Changer l'état de l'inscription de {{ $Joueur->nom . " " . $Joueur->prénom }} :</label>
												<select class="form-control" id="nouvelEtatInscription" name="nouvelEtatInscription">
													<option value="refusé">Refuser</option>
													<option value="accepté">Accepter</option>
													<option value="en attente">Mettre en attente</option>
													<option value="réglé">Réglé</option>
												</select>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
											<button type="submit" class="btn btn-primary">Enregistrer le nouvel état</button>
										</div>
									</form>
								</div>
							</div>
						</div>
						@endif
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>
@endsection