@extends('includes.template+navbar')
@section('entete')
<h1>Liste des membre du staff</h1>
<a class="btn btn-success float-right" href="{{ route('303Event.administration.nouveau.staff.formulaire') }}">Ajouter un modérateur</a>
@endsection
@section("barre.latérale.gauche")
<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
<span>Informations supplémentaires</span>
</h6>
<li class="nav-item">
	<p class="ml-5 mt-1">
		Nombre total de modérateur: {{ $Staffs->nombre }}
	</p>
</li>
@endsection
@section("contenu")
<table class="table table-hover bg-tan mt-3">
	<thead>
		<tr>
			<th scope="col">#</th>
			<th scope="col">identifiant</th>
			<th scope="col">rang</th>
			<th scope="col">Option</th>
		</tr>
	</thead>
	<tbody>
		@foreach($Staffs as $Staff)
		<tr>
			<th scope="row">{{ $Staff->id }}</th>
			<td>{{ $Staff->identifiant }}</td>
			<td>{{ $Staff->rang }}</td>
			<td>
				<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#Suppression{{$Staff->id}}">
				Supprimer
				</button>
			</td>
			
			
			<div class="modal fade" id="Suppression{{$Staff->id}}" tabindex="-1" role="dialog">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Suppression du membre du staff</h5>
						</div>
						<div class="modal-body">
							<p>Êtes-vous sûr de vouloir supprimer le membre du staff {{ $Staff->identifiant }} ?</p>
						</div>
						<div class="modal-footer">
							<form action="{{ route("303Event.administration.liste.staff.suppressionStaff", $Staff->id) }}" method="POST">
								{{ csrf_field() }}
								{{ method_field("delete") }}
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
								<button class="btn btn-danger" type="submit">Supprimer</button>
							</form>
						</div>
					</div>
				</div>
			</div>
			
		</tr>
		@endforeach()
	</tbody>
</table>
@endsection