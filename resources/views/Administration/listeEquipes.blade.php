@extends('includes.template+navbar')
@section('entete')
<h1>Liste des équipes <a class="btn btn-success float-right" href="{{ route('303Event.administration.événement.équipe.formulaire') }}">Ajouter une équipe</a></h1>
@endsection
@section("barre.latérale.gauche")
<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
<span>Informations supplémentaires</span>
</h6>
<li class="nav-item">
	<p class="ml-5 mt-1">
		Nombre total d'événements: {{ $Equipes->nombre }}
	</p>
</li>
@endsection
@section("contenu")
<table class="table table-hover table-responsive-sm bg-tan mt-3">
	<thead>
		<tr>
			<th scope="col">#</th>
			<th scope="col">Nom</th>
			<th scope="col">Description</th>
			<th scope="col">Option</th>
		</tr>
	</thead>
	<tbody>
		@foreach($Equipes as $Equipe)
		<tr>
			<th scope="row">{{ $Equipe->id }}</th>
			<td>{{ $Equipe->nom }}</td>
			<td>{{ $Equipe->description }}</td>
			<td>
				@if(DB::table('Regroupement')->where('idEquipe', $Equipe->id)->first())

				@else
				<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#Suppression{{$Equipe->id}}">
				Supprimer
				</button>
				<div class="modal fade text-body" id="Suppression{{$Equipe->id}}" tabindex="-1" role="dialog">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<form action="{{ route('303Event.administration.événement.équipe.suppression', $Equipe->id) }}" method="POST">
								@csrf
								{{ method_field("delete") }}
								<div class="modal-header">
									<h5 class="modal-title">Suppression d'équipe</h5>
								</div>
								<div class="modal-body">
									<p>Êtes-vous sûr de vouloir supprimer l'équipe <strong>{{ $Equipe->nom }}</strong> ?</p>
								</div>
								<div class="modal-footer">
									
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
									<button class="btn btn-danger" type="submit">Supprimer</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				@endif
			</td>
		</tr>
		@endforeach()
	</tbody>
</table>
@endsection