@extends('includes.template+navbar')
@section('entete')
<h1>Liste des teams <a class="btn btn-success float-right" href="{{ route('303Event.team.nouvelle.formulaire') }}">Ajouter une team</a></h1>
@endsection
@section("barre.latérale.gauche")
<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
<span>Informations supplémentaires</span>
</h6>
<li class="nav-item">
	<p class="ml-5 mt-1">
		Nombre total de teams: {{ $Teams->nombre }}
	</p>
</li>
@endsection
@section("contenu")
<table class="table table-hover bg-tan mt-3">
	<thead>
		<tr>
			<th scope="col">#</th>
			<th scope="col">Nom</th>
			<th scope="col">Ville</th>
			<th scope="col">Membres</th>
			<th scope="col">Véhicules</th>
			<th scope="col">Option</th>
		</tr>
	</thead>
	<tbody>
		@foreach($Teams as $Team)
		<tr>
			<th scope="row">{{ $Team->id }}</th>
			<td>{{ $Team->nom }}</td>
			<td>{{ $Team->ville }}</td>
			<td>
				<button type="button" class="btn btn-info" data-toggle="modal" data-target="#listeMembres{{$Team->id}}">
				{{ $Team->Membres->nombre }}
				</button>
				<div class="modal fade text-body" id="listeMembres{{$Team->id}}" tabindex="-1" role="dialog">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">Liste des membres la team: {{$Team->nom}}</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</div>
							<div class="modal-body">
								<table class="table table-hover table-responsive">
									<thead>
										<tr>
											<th scope="col">id</th>
											<th scope="col">Nom</th>
											<th scope="col">Prénom</th>
											<th scope="col">Pseudo</th>
											<th scope="col">Option</th>
										</tr>
									</thead>
									<tbody>
										@foreach($Team->Membres as $Membre)
										<form action="{{ route('303Event.administration.team.éjecter.joueur', $Membre->id) }}" method="POST">
											@csrf
											{{ method_field("delete") }}
											<tr>
												<th scope="row">{{ $Membre->id }}</th>
												<td>{{ $Membre->nom }}</td>
												<td>{{ $Membre->prénom }}</td>
												<td>{{ $Membre->pseudo }}</td>
												<td><button class="btn btn-danger" type="submit">Ejecter de la team</button></td>
											</tr>
										</form>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</td>
			<td>
				<button type="button" class="btn btn-info" data-toggle="modal" data-target="#listeVéhicules{{$Team->id}}">
				{{ $Team->Véhicules->nombre }}
				</button>
				<div class="modal fade text-body" id="listeVéhicules{{$Team->id}}" tabindex="-1" role="dialog">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">Liste des véhicules la team: {{$Team->nom}}</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</div>
							<div class="modal-body">
								<table class="table table-hover">
									<thead>
										<tr>
											<th scope="col">id</th>
											<th scope="col">Nom</th>
											<th scope="col">Type</th>
											<th scope="col">Joueur</th>
										</tr>
									</thead>
									<tbody>
										@foreach($Team->Véhicules as $Véhicule)
										<tr>
											<th scope="row">{{ $Véhicule->id }}</th>
											<td>{{ $Véhicule->nom }}</td>
											<td>{{ $Véhicule->type }}</td>
											<td>{{ $Véhicule->joueur }}</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</td>
			<td>
				@if(Session::get('Staff')->rang == "Administrateur")
				<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#Suppression{{$Team->id}}">
				Supprimer
				</button>
				<div class="modal fade" id="Suppression{{$Team->id}}" tabindex="-1" role="dialog">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<form action="{{ route("303Event.administration.team.suppression", $Team->id) }}" method="POST">
								@csrf
								{{ method_field("delete") }}
								<div class="modal-header">
									<h5 class="modal-title">Suppression de team</h5>
								</div>
								<div class="modal-body">
									<p>Êtes-vous sûr de vouloir supprimer la team <strong>{{ $Team->nom }}</strong> ?</p>
									<div class="form-group pt-3">
										<label for="motDePasse">Mot de passe:</label>
										<input type="password" class="form-control" id="motDePasse" name="motDePasse">
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
								<button class="btn btn-danger" type="submit">Supprimer</button></div>
							</form>
						</div>
					</div>
				</div>
				@else
				<button type="button" class="btn btn-danger" disabled>Supprimer</button>
				@endif
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
@endsection