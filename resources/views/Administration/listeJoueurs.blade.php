@extends('includes.template+navbar')
@section('entete')
<h1>Liste des joueurs</h1>
@if(Session::get('Staff')->rang == "Administrateur")
<a class="btn btn-dark row float-sm-right float-none my-sm-0 mb-2" href="{{ route('303Event.administration.joueur.récupération.demande.formulaire') }}">Récupération de compte</a>
@endif
<form class="row" action="{{ route("303Event.administration.liste.joueurs.recherche") }}" method="post">
	@csrf
	<div class="form-inline">
		<select class="custom-select mr-sm-2" name="attribut">
			<option value="" selected>Attribut recherché</option>
			<option value="id">id</option>
			<option value="nom">nom</option>
			<option value="prénom">prénom</option>
			<option value="pseudo">pseudo</option>
			<option value="team">team</option>
			<option value="adresse">adresse</option>
			<option value="ville">ville</option>
			<option value="codePostal">codePostal</option>
			<option value="numTel">numTel</option>
			<option value="email">email</option>
			<option value="numSécuritéSocial">numSécuritéSocial</option>
			<option value="cléContrôleSécuritéSocial">cléContrôleSécuritéSocial</option>
		</select>
		<input type="text" class="form-control mr-sm-2" name="recherche" placeholder="Entrer la recherche">
		<button type="submit" class="btn btn-primary mx-sm-2">Lancer la recherche</button>
	</div>
</form>
@endsection
@section("barre.latérale.gauche")
<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
<span>Informations supplémentaires</span>
</h6>
<li class="nav-item">
	<p class="ml-5 mt-1">
		Nombre total de joueurs: {{ $Joueurs->nombre }}
	</p>
</li>
@endsection
@section("contenu")
<table class="table table-hover bg-tan mt-3">
	<thead>
		<tr>
			<th scope="col">#</th>
			<th scope="col">Nom</th>
			<th scope="col">Prénom</th>
			<th scope="col">Date de naissance</th>
			<th scope="col">Pseudo</th>
			<th scope="col">Team</th>
			<th scope="col">Adresse complète</th>
			<th scope="col">Numéro de téléphone</th>
			<th scope="col">Adresse Email</th>
			<th scope="col">Numéro de sécurité social (NIR)</th>
			<th scope="col">Clé de contrôle</th>
			<th scope="col">Moyenne</th>
			<th scope="col">Options</th>
		</tr>
	</thead>
	<tbody>
		@foreach($Joueurs as $Joueur)
		@if(now() >= $Joueur->dateExpiration) {{-- Joueur expiré--}}
		<tr class="bg-secondary">
			@elseif(now()->diffInYears($Joueur->dateDeNaissance) < 18) {{-- Joueur mineur--}}
			<tr class="bg-warning">
				@else
				<tr>
					@endif
					<th scope="row">{{ $Joueur->id }}</th>
					<td>{{ $Joueur->nom }}</td>
					<td>{{ $Joueur->prénom }}</td>
					<td>{{ date('d-m-Y',strtotime($Joueur->dateDeNaissance)) }}</td>
					<td>{{ $Joueur->pseudo }}</td>
					@if($Joueur->idTeam)
					<td>{{ DB::table('Team')->where('id', $Joueur->idTeam)->first()->nom }}</td>
					@else
					<td></td>
					@endif
					<td>{{ DB::table('Habitation')->where('id', $Joueur->idHabitation)->first()->adresse . " " . DB::table('Habitation')->where('id', $Joueur->idHabitation)->first()->ville . " " . DB::table('Habitation')->where('id', $Joueur->idHabitation)->first()->codePostal}}</td>
					<td>{{ $Joueur->numTel }}</td>
					<td>{{ $Joueur->email }}</td>
					<td>{{ $Joueur->numSécuritéSocial }}</td>
					<td>{{ $Joueur->cléContrôleSécuritéSocial }}</td>
					<td>
						@if((DB::table('Note')->where('idJoueur', $Joueur->id)->first()))
						<button type="button" class="btn btn-light mb-2" data-toggle="modal" data-target="#Notes{{$Joueur->id}}">{{ $Joueurs->Controller->moyenne($Joueur->id) }}</button>
						<div class="modal fade" id="Notes{{$Joueur->id}}" tabindex="-1" role="dialog">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title">Notes du joueur</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<p>{{ $Joueur->nom . " " . $Joueur->prénom }}<br>{{ $Joueurs->Controller->notes($Joueur->id) }}</p>
									</div>
									<div class="modal-footer">
										<form action="{{ route("303Event.administration.notation.actualiser") }}" method="POST">
											{{ csrf_field() }}
											<input type="hidden" name="idJoueur" value="{{$Joueur->id}}">
											<div class="form-group">
												<label for="">Role-play:</label>
												<select class="form-control" id="RolePlay" name="noteRolePlay">
													<option value="{{ $Joueurs->Controller->note($Joueur->id, "Role-play") }}">{{ $Joueurs->Controller->note($Joueur->id, "Role-play") }}</option>
													<option value="0">0</option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">5</option>
												</select>
											</div>
											<div class="form-group">
												<label for="ponctualité">Ponctualité:</label>
												<select class="form-control" id="ponctualité" name="notePonctualité">
													<option value="{{ $Joueurs->Controller->note($Joueur->id, "Role-play") }}">{{ $Joueurs->Controller->note($Joueur->id, "Ponctualité") }}</option>
													<option value="0">0</option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">5</option>
												</select>
											</div>
											<div class="form-group">
												<label for="FairPLay">Fair-play:</label>
												<select class="form-control" id="FairPLay" name="noteFairPlay">
													<option value="{{ $Joueurs->Controller->note($Joueur->id, "Role-play") }}">{{ $Joueurs->Controller->note($Joueur->id, "Fair-play") }}</option>
													<option value="0">0</option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">5</option>
												</select>
											</div>
											<input class="btn btn-primary" type="submit">
										</form>
									</div>
								</div>
							</div>
						</div>
						@else
						<button type="button" class="btn btn-info mb-2" data-toggle="modal" data-target="#Noter{{$Joueur->id}}">
						Noter
						</button>
						<div class="modal fade" id="Noter{{$Joueur->id}}" tabindex="-1" role="dialog">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<form action="{{ route("303Event.administration.notation") }}" method="POST">
										@csrf
										<input type="hidden" name="idJoueur" value="{{$Joueur->id}}">
										<div class="modal-header">
											<h5 class="modal-title">Noter le joueur {{ $Joueur->nom . " " . $Joueur->prénom }}</h5>
										</div>
										<div class="modal-body">
											<div class="form-group">
												<label for="">Role-play:</label>
												<select class="form-control" id="RolePlay" name="noteRolePlay">
													<option value="">Non évaluable</option>
													<option value="0">0</option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">5</option>
												</select>
											</div>
											<div class="form-group">
												<label for="ponctualité">Ponctualité:</label>
												<select class="form-control" id="ponctualité" name="notePonctualité">
													<option value="">Non évaluable</option>
													<option value="0">0</option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">5</option>
												</select>
											</div>
											<div class="form-group">
												<label for="FairPLay">Fair-play:</label>
												<select class="form-control" id="FairPLay" name="noteFairPlay">
													<option value="">Non évaluable</option>
													<option value="0">0</option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">5</option>
												</select>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
											<button class="btn btn-primary" type="submit">Valider les notes</button>
										</div>
									</form>
								</div>
							</div>
						</div>
						@endif
					</td>
					<td>
						@if(!(DB::table('Ban')->where('idJoueur', $Joueur->id)->first()))
						<button type="button" class="btn btn-danger mb-2" data-toggle="modal" data-target="#Ban{{$Joueur->id}}">
						Bannir
						</button>
						<div class="modal fade" id="Ban{{$Joueur->id}}" tabindex="-1" role="dialog">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<form action="{{ route("303Event.administration.banJoueur") }}" method="POST">
										{{ csrf_field() }}
										<input type="hidden" name="idJoueur" value="{{$Joueur->id}}">
										<div class="modal-header">
											<h5 class="modal-title">Ban de joueurs</h5>
										</div>
										<div class="modal-body">
											<p class="text-danger">Bannir {{ $Joueur->nom . " " . $Joueur->prénom }}</p>
											<div class="form-group">
												<label for="raisonBannissement{{$Joueur->id}}">Raison du bannissement:</label>
												<textarea class="form-control" id="raisonBannissement{{$Joueur->id}}" name="raisonBannissement" rows="3"></textarea>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
											<button class="btn btn-danger" type="submit">Bannir</button>
										</div>
									</form>
								</div>
							</div>
						</div>
						@else
						<button type="button" class="btn btn-warning mb-2" data-toggle="modal" data-target="#Déban{{$Joueur->id}}">
						Débannir
						</button>
						<div class="modal fade" id="Déban{{$Joueur->id}}" tabindex="-1" role="dialog">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title">Déban de joueur</h5>
									</div>
									<div class="modal-body">
										<p>Êtes-vous sûr de vouloir débannir le joueur <strong>{{ $Joueur->nom . " " . $Joueur->prénom }}</strong> ?</p>
									</div>
									<div class="modal-footer">
										<form action="{{ route("303Event.administration.banJoueur.déban") }}" method="POST">
											{{ csrf_field() }}
											<input type="hidden" name="idJoueur" value="{{$Joueur->id}}">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
											<button class="btn btn-warning" type="submit">Débannir</button>
										</form>
									</div>
								</div>
							</div>
						</div>
						@endif
						<br>
						@if(Session::get('Staff')->rang == "Administrateur")
						<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#Suppression{{$Joueur->id}}">
						Supprimer
						</button>
						@else
						<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#Suppression{{$Joueur->id}}" disabled>
						Supprimer
						</button>
						@endif
						@if(now() >= $Joueur->dateExpiration) {{-- Joueur expiré--}}
						<button type="button" class="btn btn-dark mt-2" data-toggle="modal" data-target="#Expiration{{$Joueur->id}}">Expirer le {{ date('d-m-Y',strtotime($Joueur->dateExpiration)) }}</button>
						@endif
					</td>
					
					<div class="modal fade" id="Suppression{{$Joueur->id}}" tabindex="-1" role="dialog">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<form action="{{ route("303Event.administration.liste.joueurs.suppression", $Joueur->id) }}" method="POST">
									@csrf
									{{ method_field("delete") }}
									<div class="modal-header">
										<h5 class="modal-title">Suppression de joueur</h5>
									</div>
									<div class="modal-body">
										<p>Êtes-vous sûr de vouloir supprimer le joueur <strong>{{ $Joueur->nom . " " . $Joueur->prénom }}</strong> ?</p>
										<div class="form-group pt-3">
											<label for="motDePasse">Mot de passe:</label>
											<input type="password" class="form-control" id="motDePasse" name="motDePasse">
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
									<button class="btn btn-danger" type="submit">Supprimer</button></div>
								</form>
							</div>
						</div>
					</div>
					@if(now() >= $Joueur->dateExpiration) {{-- Joueur expiré--}}
					<div class="modal fade" id="Expiration{{$Joueur->id}}" tabindex="-1" role="dialog">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<form action="{{ route("303Event.administration.liste.joueurs.expiration", $Joueur->id) }}" method="POST">
									@csrf
									<div class="modal-header">
										<h5 class="modal-title">Expiration de joueur</h5>
									</div>
									<div class="modal-body">
										<p>Êtes-vous sûr de vouloir expirer les données du joueur <strong>{{ $Joueur->nom . " " . $Joueur->prénom }}</strong> ?</p>
										<div class="form-group pt-3">
											<label for="motDePasse">Mot de passe:</label>
											<input type="password" class="form-control" id="motDePasse" name="motDePasse">
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
										<button class="btn btn-danger" type="submit">Expirer</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					@endif
				</tr>
				@endforeach
			</tbody>
		</table>
		@endsection