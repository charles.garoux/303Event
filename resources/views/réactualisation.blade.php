@extends('includes.template')
@section('entete')
<h3>Réactualisation des informations</h3>
@endsection
@section('contenu')
<div class="card col-12 bg-danger mb-3">
	<div class="card-body">
		<strong>Attention: Les champs dont les libellés sont en caractères gras sont obligatoires.</strong>
	</div>
</div>
<form class="row" method="post">
	@csrf
	<div class="col-xl-6 col-md-12 col-sm-12 col-12">
		<div class="card shadow-lg mb-5 bg-tan rounded">
			<div class="card-header">
				Informations personnel
			</div>
			<div class="card-body">
				<div class="form-group row">
					<label for="nom" class="col-sm-2 col-form-label" ><strong>Nom:</strong></label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="nom" id="nom" maxlength="32" required/>
					</div>
				</div>
				<div class="form-group row">
					<label for="prénom" class="col-sm-2 col-form-label" ><strong>Prénom:</strong></label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="prénom" id="prénom" maxlength="32" required/>
					</div>
				</div>
				<div class="form-group row">
					<label for="dateDeNaissance" class="col-sm-3 col-form-label" ><strong>Date de naissance:</strong></label>
					<div class="col-sm-9">
						<input type="date" class="form-control" name="dateDeNaissance" id="dateDeNaissance" placeholder="Date de naissance" required/>
					</div>
				</div>
				<div class="form-group row">
					<label for="numSécuritéSocial" class="col-sm-4 col-form-label" ><strong>Numéro de sécurité social:</strong></label>
					<div class="col-sm-8">
						<input type="text" class="form-control" name="numSécuritéSocial" id="numSécuritéSocial" maxlength="13" required/>
					</div>
				</div>
				<div class="form-group row">
					<label for="cléContrôleSécuritéSocial" class="col-sm-4 col-form-label" ><a title="Ce sont les 2 derniers chiffre en bas"><strong>Clé de contrôle</a>:</strong></label>
					<div class="col-sm-8">
						<input type="text" class="form-control" name="cléContrôleSécuritéSocial" id="cléContrôleSécuritéSocial" maxlength="2" required/>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xl-6 col-md-12 col-sm-12 col-12">
		<div class="card shadow-lg mb-5 bg-tan rounded">
			<div class="card-header">
				Informations de contacte
			</div>
			<div class="card-body">
				<div class="form-group row">
					<label for="email" class="col-sm-3 col-form-label" >Adresse E-mail:</label>
					<div class="col-sm-9">
						<input type="email" class="form-control" name="email" id="email" maxlength="64" />
					</div>
				</div>
				<div class="form-group row">
					<label for="numTel" class="col-sm-3 col-form-label" >Numéro de téléphone:</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="numTel" id="numTel" maxlength="10" />
					</div>
				</div>
				<div class="form-group">
					<label for="" class="form-label" ><strong>Lieu d'habitation:</strong></label>
					<div class="row">
						<input type="text" class="form-control col-sm-4 col-12" name="adresse" id="adresse" placeholder="Adresse" maxlength="32" required/>
						<input type="text" class="form-control col-sm-4 col-12" name="ville" id="ville" placeholder="Ville" maxlength="32" required/>
						<input type="text" class="form-control col-sm-4 col-12" name="codePostal" id="codePostal" placeholder="Code Postal" maxlength="5" required/>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	<div class="col-xl-6 col-md-12 col-sm-12 col-12">
		<div class="card shadow-lg mb-5 bg-tan rounded">
			<div class="card-header">
				Informations d'airsoft
			</div>
			<div class="card-body">
				<div class="form-group row">
					<label for="pseudo" class="col-sm-3 col-form-label" >Pseudo:</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="pseudo" id="pseudo" maxlength="32" />
					</div>
				</div>
				<div class="form-group row">
					<label for="team" class="col-sm-3 col-form-label" >Team:</label>
					<div class="col-sm-9">
						<select type="text" class="form-control" name="idTeam" id="team">
							<option value="">Je ne souhaite pas l'indiquer</option>
							@foreach($Teams as $Team)
							<option value="{{ $Team-> id }}">{{ $Team->nom }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="checkbox" name="nouvelleTeam" value="oui" id="créationTeam">
					<label class="form-check-label" for="créationTeam">
						Ma team n'est pas dans la liste mais je souhaite l'ajouter
						<br>
						<small class="form-text text-muted">Si sélectionné, le choix dans "Team" sera ignoré.<br>
						Après l'inscription la page suivante vous permettra d'ajouter votre team.</small>
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xl-6 col-md-12 col-sm-12 col-12">
		<div class="card shadow-lg mb-5 bg-tan rounded">
			<div class="card-header">
				Informations d'identification du compte
			</div>
			<div class="card-body">
				<div class="form-group row">
					<label for="identifiant" class="col-sm-3 col-form-label" ><strong>Identifiant:</strong></label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="identifiant" id="identifiant" maxlength="32" required/>
					</div>
				</div>
				<div class="form-group row">
					<label for="motDePasse" class="col-sm-3 col-form-label" ><strong>Mot de passe:</strong></label>
					<div class="col-sm-9">
						<input type="password" class="form-control" name="motDePasse" id="motDePasse" maxlength="32" required/>
					</div>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="checkbox" value="accepté" id="conditionsUtilisation" name="conditionsUtilisation" required>
					<label class="form-check-label" for="conditionsUtilisation">
						<strong>J'accepte<a href="{{ route('303Event.informations.conditionsUtilisation') }}" target="blank"> les conditions d'utilisation de 303Event</a></strong>
					</label>
				</div>
				<button type="submit" class="btn btn-primary btn-block">Enregistrer mon compte</button>
			</div>
		</div>
	</div>
</div>
</form>
@endsection