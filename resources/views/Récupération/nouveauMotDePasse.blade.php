@extends('includes.template')
@section('entete')
<h3>Récupération de compte</h3>
@endsection
@section('contenu')
<div class="row justify-content-center">
    <div class="col-xl-4 col-md-6 col-sm-12 col-12">
        <div class="card shadow-lg mb-5 bg-tan rounded">
            <div class="card-header text-center">
                {{$Joueur->nom . " " . $Joueur->prénom}}, vous pouvez entrer un nouveau mot de passe
            </div>
            <div class="card-body">
                <form action="" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="nouveauMotDePasse">Nouveau mot de passe*:</label>
                        <input type="password" class="form-control" id="nouveauMotDePasse" name="nouveauMotDePasse" required>
                    </div>
                    <p><small>Petit rappelle: votre identifiant à été envoyé par e-mail.</small></p>
                    <button type="submit" class="btn btn-primary">Envoyer ma requête</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection