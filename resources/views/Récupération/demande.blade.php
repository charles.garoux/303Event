@extends('includes.template')
@section('entete')
<h3>Demande de récupération de compte</h3>
@endsection
@section('contenu')
<div class="row justify-content-center">
    <div class="col-xl-4 col-md-6 col-sm-12 col-12">
        <div class="card shadow-lg mb-5 bg-tan rounded">
            <div class="card-header text-center">
                Informations de demande de récupération
            </div>
            <div class="card-body">
                <form action="" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="email">Mon adresse e-mail*:</label>
                        <input type="email" class="form-control" id="email" name="email" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Envoyer ma requête</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection