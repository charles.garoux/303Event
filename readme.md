# 303Event
Ce **projet de stage de BTS SIO** a été développé pour mon stage de première année de BTS SIO, dans l'association "303 airsoft team".  

Au cours d'un entretien avec mon maître de stage j'ai pu connaître les besoins de l'association. J'ai produit l'analyse pour faire un schéma entité/association afin de développer l'application web demandé par l'association.

Ce projet a été développé en **télétravail** depuis chez moi ce qui m'a permis d'avoir un environnement de travail familier.

## Objectif

Permettre a l'association de gérer leurs événements ainsi que les informations sur les participants par un outil unique. Leurs outils étant Facebook et un tableur.

## Résultat
L'application a été développée avec les frameworks **Laravel** et **Boostrap** mais l'application n'a pas pu être mise en production, car elle nécessitait des démarches administratives auprès de la CNIL.

>PS: Ce projet était mon deuxième projet Web, je l'ai conçue pour être repris par un débutant comme moi.
